package com.gigakorea.giganumbericdomain;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.gigakorea.giganumbericdomain.Entity.LoginEntity;
import com.rs.mobile.common.AppConfig;
import com.rs.mobile.common.C;
import com.rs.mobile.common.S;
import com.rs.mobile.common.T;
import com.rs.mobile.common.network.OkHttpHelper;
import com.rs.mobile.common.util.EncryptUtils;
import com.rs.mobile.common.util.GsonUtils;
import com.rs.mobile.common.util.Util;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Request;

public class LoginActivity extends Activity {

    private ImageView quitView;
    private Button loginBtn;
    private EditText idEditText, pwEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initview();
        listen();
    }

    private void listen() {

        quitView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String id = idEditText.getText().toString();

                String pw = pwEditText.getText().toString();

                if (id == null || id.equals("")) {

                    T.showToast(LoginActivity.this, getString(com.rs.mobile.common.R.string.msg_empty_id));
                    return;

                }

                if (pw == null || pw.equals("")) {

                    T.showToast(LoginActivity.this, getString(com.rs.mobile.common.R.string.msg_empty_pw));

                    return;

                }

                login(id, pw);
            }
        });


    }

    private void login(String id, String pw) {
        HashMap<String, String> params = new HashMap<>();
        params.put("lang_type", AppConfig.LANG_TYPE);
        params.put("memid", id);
        params.put("mempwd", EncryptUtils.SHA512(pw));
        params.put("deviceNo", Util.getDeviceId(LoginActivity.this));

        OkHttpHelper okHttpHelper = new OkHttpHelper(LoginActivity.this);
        okHttpHelper.addPostRequest(new OkHttpHelper.CallbackLogic() {
            @Override
            public void onBizSuccess(String responseDescription, JSONObject data, String flag) {

                LoginEntity entity = GsonUtils.changeGsonToBean(responseDescription, LoginEntity.class);
                if ("1".equals(entity.status)) {
                    S.set(LoginActivity.this, C.KEY_JSON_CUSTOM_CODE, entity.custom_code);
                    S.setShare(LoginActivity.this, C.KEY_JSON_CUSTOM_CODE, entity.custom_code);
                    S.set(LoginActivity.this, C.KEY_REQUEST_MEMBER_ID, entity.custom_code);
                    S.set(LoginActivity.this, C.KEY_JSON_CUSTOM_ID, entity.custom_id);
                    S.set(LoginActivity.this, C.KEY_JSON_CUSTOM_NAME, entity.custom_name);
                    S.set(LoginActivity.this, C.KEY_JSON_NICK_NAME, entity.nick_name);
                    S.setShare(LoginActivity.this, C.KEY_JSON_TOKEN, entity.token + "|" + entity.ssoId + "|" + entity.custom_code + "|" + Util.getDeviceId(LoginActivity.this));
                    S.set(LoginActivity.this, C.KEY_JSON_TOKEN, entity.token + "|" + entity.ssoId + "|" + entity.custom_code + "|" + Util.getDeviceId(LoginActivity.this));
                    S.set(LoginActivity.this, C.KEY_JSON_PROFILE_IMG, entity.profile_img);
                    S.set(LoginActivity.this, C.KEY_JSON_DIV_CODE, entity.div_code);
                    S.set(LoginActivity.this, C.KEY_JSON_SSOID, entity.ssoId);

                    S.set(LoginActivity.this, C.KEY_JSON_SSO_REGIKEY, entity.sso_regiKey);
                    S.set(LoginActivity.this, C.KEY_JSON_MALL_HOME_ID, entity.mall_home_id);
                    S.set(LoginActivity.this, C.KEY_JSON_POINT_CARD_NO, entity.point_card_no);
                    S.set(LoginActivity.this, C.KEY_JSON_PARENT_ID, entity.parent_id);

//					AppConfig.custom_code = entity.custom_code;
//					AppConfig.custom_id = entity.custom_id;
//					AppConfig.custom_name = entity.custom_name;
//					AppConfig.nick_name = entity.nick_name;
//					AppConfig.token = entity.token + "|aaaaaa|" + entity.custom_code;
//					AppConfig.profile_img = entity.profile_img;
//					AppConfig.div_code = entity.div_code;
//					AppConfig.ssoId = entity.ssoId;
//					AppConfig.sso_regiKey = entity.sso_regiKey;
//					AppConfig.mall_home_id = entity.mall_home_id;
//					AppConfig.point_card_no = entity.point_card_no;
//					AppConfig.parent_id = entity.parent_id;
//
//					S.setShare(LoginActivity.this, C.KEY_JSON_TOKEN, entity.token);

                    Toast.makeText(LoginActivity.this, entity.msg, Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, entity.msg, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onBizFailure(String responseDescription, JSONObject data, String flag) {
            }

            @Override
            public void onNetworkError(Request request, IOException e) {
            }
        }, "http://member.gigawon.cn:8808/api/Login/requestLoginCheck", GsonUtils.createGsonString(params));
    }


    private void initview() {
        idEditText = (EditText) findViewById(com.rs.mobile.common.R.id.id_edt_text);
        pwEditText = (EditText) findViewById(com.rs.mobile.common.R.id.pw_edt_text);
        loginBtn = (Button) findViewById(com.rs.mobile.common.R.id.login_btn);
        quitView = findViewById(R.id.img_quit);
    }
}
