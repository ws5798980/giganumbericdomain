package com.gigakorea.giganumbericdomain;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

final class UserInfoDataClass2 {

    public String status, flag, msg, ssoId, sso_regiKey, custom_id, custom_code, token, profile_img, process_result, result_msg;
    public String deviceNo, div_code, store_code, mall_home_id, message, memid, point_card_no, parent_id, parent_mall_domain, grade;
    public String grade_name, sales_id, minfo, visit_sub_domain, visit_domain, my_sub_domain, my_domain, my_sales_level, my_nation_code, my_group_code, my_authYN, my_authKey;

    public void UserInfoDataClass()
    {
        status = "";
        flag = "";
        msg = "";
        ssoId = "";
        sso_regiKey = "";
        custom_id = "";
        custom_code = "";
        token = "";
        profile_img = "";
        process_result = "";
        result_msg = "";
        deviceNo = "";
        div_code = "";
        store_code = "";
        mall_home_id = "";
        message = "";
        memid = "";
        point_card_no = "";
        parent_id = "";
        parent_mall_domain = "";
        grade = "";
        grade_name = "";
        sales_id = "";
        minfo = "";
        visit_sub_domain = "";
        visit_domain = "";
        my_sub_domain = "";
        my_domain = "";
        my_sales_level = "";
        my_nation_code = "";
        my_group_code = "";
        my_authYN = "";
        my_authKey = "";
    }
}

public class MyMainActivity extends Activity {

    private Integer tb_totalCount = 16;
    private Integer side_totalCount = 10;
    private Integer main_totalCount = 140;
    private ImageView[] iv_topH = new ImageView[tb_totalCount];
    private ImageView[] iv_bottomH = new ImageView[tb_totalCount];
    private ImageView[] iv_leftH = new ImageView[side_totalCount];
    private ImageView[] iv_rightH = new ImageView[side_totalCount];
    private TextView[] iv_topCollect = new TextView[tb_totalCount];
    private TextView[] tv_topCollect2 = new TextView[side_totalCount];
    private TextView[] tv_topCollect3 = new TextView[side_totalCount];
    private TextView[] tv_topCollect4 = new TextView[tb_totalCount];
    private TextView tv_dial_btn;

    /* UserInformation Variable */

    private TextView tv_txt_TodayAccount, tv_txt_yesterday;

    private String[] pri_ad_url = new String[main_totalCount+1];

    //Main Number
    private ImageView[] iv_MainMenu = new ImageView[main_totalCount];

    public ScrollView sv_TopNumber1, sv_TopNumber2, sv_TopNumber3, sv_TopNumber4;

    public String[] ret_TotalNumber = new String[4];

    public UserInfoDataClass uic = new UserInfoDataClass();

    private final Handler mHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 0)
            {
                //Toast.makeText(getApplicationContext(), msg.obj.toString(), Toast.LENGTH_SHORT).show();
                String ret = httpPost(ret_TotalNumber[0], ret_TotalNumber[1], ret_TotalNumber[2], ret_TotalNumber[3]);
                JsonParsingMain(ret);
            } else if(msg.what == 1) {
                String ret = httpPost(ret_TotalNumber[0], ret_TotalNumber[1], ret_TotalNumber[2], ret_TotalNumber[3]);
                JsonParsingMain(ret);
            }
        }
    };

    private final Handler mHandler1 = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 0)
            {
                ButtonClickChangager(msg.obj.toString());
            }
        }
    };



    public int READ_TIMEOUT = 5;
    public String seq_no = "";
    public String t_num = "";
    public String l_num = "";
    public String r_num = "";
    public String b_num = "";
    public String c_num = "";
    public String ad_num = "";
    public String ad_url = "";

    public int global_i = 0;

    public String LoginURL = "http://join.gigaroom.cn:8082/10_Member/login";

    private LinearLayout lllogin, llcharge, llcontents,llmyDoamin,llmain;
    private ImageView imglogin, imgcharge, imgcontents;
    private TextView tvlogin, txtcharge, txtcontents;

    public JSONObject resultJsonData=null;


    private void JsonParsingMain(String JsonData)
    {

        String resourceStringID = "";
        int resID = 0;
        String num = "";
        String packName = this.getPackageName();


        for(int i=0; i < pri_ad_url.length; i++)
        {
            pri_ad_url[i] = "";
        }

        try {
            for(int i = 0; i < main_totalCount; i++)
            {
                try {
                    if (Integer.toString(i + 1).length() == 1) {
                        num = "0" + Integer.toString(i + 1);
                    } else {
                        num = Integer.toString(i + 1);
                    }

                    resourceStringID = "n" + num;
                    resID = getResources().getIdentifier(resourceStringID, "drawable", packName);
                    BitmapDrawable img;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                    } else {
                        img = (BitmapDrawable) getResources().getDrawable(resID);
                    }
                    iv_MainMenu[i].setImageDrawable(img);

                    iv_MainMenu[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
//                            intent.putExtra("url", LoginURL);
//                            startActivityForResult(intent, 1000);
                        }
                    });
                }catch (Exception ee){}
            }

            JSONObject status_json = new JSONObject(JsonData);
            if(status_json.getString("status").compareTo("1") == 0) {

                JSONObject tot_info = new JSONObject(JsonData).getJSONObject("info");
                tv_txt_TodayAccount.setText("오늘 가입 회원수 : " + tot_info.getString("today_mem_count") + " / 총 회원 : " + tot_info.getString("tot_mem_count"));
                tv_txt_yesterday.setText("어제 가입 회원수 : " + tot_info.getString("yesterday_mem_count"));

                JSONArray obj = new JSONObject(JsonData).getJSONArray("data");
                for (int i = 0; i < obj.length(); i++) {


                    try {
                        JSONObject jObject = obj.getJSONObject(i);
                        seq_no = jObject.optString("seq_no");
                        t_num = jObject.optString("t_num");
                        l_num = jObject.optString("l_num");
                        r_num = jObject.optString("r_num");
                        b_num = jObject.optString("b_num");
                        c_num = jObject.optString("c_num");
                        ad_num = jObject.optString("ad_num");
                        ad_url = jObject.optString("ad_url");

                        pri_ad_url[i] = ad_url;

                        if (c_num.length() == 1) {
                            num = "0" + c_num;
                        } else {
                            num = c_num;
                        }

                        global_i = i;

                        resourceStringID = "n" + num + "_s";
                        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);
                        BitmapDrawable img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                        } else {
                            img = (BitmapDrawable) getResources().getDrawable(resID);
                        }
                        iv_MainMenu[Integer.parseInt(c_num) - 1].setImageDrawable(img);
                        iv_MainMenu[Integer.parseInt(c_num) - 1].setTag((int) i);
                        iv_MainMenu[Integer.parseInt(c_num) - 1].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
                                intent.putExtra("url", pri_ad_url[(int) v.getTag()]);
                                //Toast.makeText(getApplicationContext(), pri_ad_url[(int) v.getTag()], Toast.LENGTH_SHORT).show();
                                startActivity(intent);
                            }
                        });
                    }catch (Exception ee){}


                }
            } else {
                Toast.makeText(getApplicationContext(), status_json.getString("msg"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException je) {
            je.printStackTrace();
        }

    }


    public String httpPost(String topNumber, String leftNumber, String rightNumber, String bottomNumber)
    {
        String retvalue = "";
        try
        {
            //java.net.URLEncoder.encode(new String(param.getByte("euc-kr")),"euc-kr")

            String Addr = "http://apiAD.gigaroom.cn:9280/api/apiSpecialAD/requestMyMainInfo";
            URL url = new URL(Addr);
            //String param = "lang_type=kor&memid=&token=t_num=" + topNumber + "&l_num=" + leftNumber + "&r_num=" + rightNumber + "&b_num=" + bottomNumber + "&visit_sub_domain=www&visit_domain=gigaroom.cn";

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod( "POST" );
            httpURLConnection.setReadTimeout(READ_TIMEOUT * 1000);
            httpURLConnection.setRequestProperty("Cache_Control", "no-cache");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);

            String json_Request_Data = "{lang_type:'kor',memid:'"+uic.custom_code+"',token:'"+uic.token+"',t_num:'" + topNumber + "',l_num:'" + leftNumber + "',r_num:'" + rightNumber + "',b_num:'" + bottomNumber + "',visit_sub_domain:'"+uic.visit_sub_domain+"',visit_domain_id:'"+uic.custom_code+"',visit_domain:'"+uic.visit_domain+"',visit_domain_code:'"+uic.visit_sub_domain+"'}";

            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(json_Request_Data.getBytes());
            outputStream.flush();
            outputStream.close();
            httpURLConnection.connect();

            String temp = null;

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            while((temp = bufferedReader.readLine()) != null){
                retvalue+= temp;
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return retvalue;
    }

    private ImageView iv_tempTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sp = getSharedPreferences("giga_userinfo", Context.MODE_PRIVATE);
        try {
            resultJsonData = new JSONObject(sp.getString("userinfo",""));
            uic.status = resultJsonData.getString("status");
            uic.flag = resultJsonData.getString("flag");
            uic.msg = resultJsonData.getString("msg");
            uic.ssoId = resultJsonData.getString("ssoId");
            uic.sso_regiKey = resultJsonData.getString("sso_regiKey");
            uic.custom_id = resultJsonData.getString("custom_id");
            uic.custom_code = resultJsonData.getString("custom_code");
            uic.token = resultJsonData.getString("token");
            uic.profile_img = resultJsonData.getString("profile_img");
            uic.process_result = resultJsonData.getString("process_result");
            uic.result_msg = resultJsonData.getString("result_msg");
            uic.deviceNo = resultJsonData.getString("deviceNo");
            uic.store_code = resultJsonData.getString("store_code");
            uic.mall_home_id = resultJsonData.getString("mall_home_id");
            uic.message = resultJsonData.getString("message");
            uic.memid = resultJsonData.getString("memid");
            uic.point_card_no = resultJsonData.getString("point_card_no");
            uic.parent_id = resultJsonData.getString("parent_id");
            uic.parent_mall_domain = resultJsonData.getString("parent_mall_domain");
            uic.grade = resultJsonData.getString("grade");
            uic.grade_name = resultJsonData.getString("grade_name");
            uic.sales_id = resultJsonData.getString("sales_id");
            uic.minfo = resultJsonData.getString("minfo");
            uic.visit_sub_domain = resultJsonData.getString("visit_sub_domain");
            uic.visit_domain = resultJsonData.getString("visit_domain");
            uic.my_sub_domain = resultJsonData.getString("my_sub_domain");
            uic.my_domain = resultJsonData.getString("my_domain");
            uic.my_sales_level = resultJsonData.getString("my_sales_level");
            uic.my_nation_code = resultJsonData.getString("my_nation_code");
            uic.my_group_code = resultJsonData.getString("my_group_code");
            uic.my_authYN = resultJsonData.getString("my_authYN");
            uic.my_authKey = resultJsonData.getString("my_authKey");

        } catch (JSONException ex) {
            resultJsonData=null;
            finish();
        }

        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());

        sv_TopNumber1 = (ScrollView)findViewById(R.id.TopNumber1);
        sv_TopNumber2 = (ScrollView)findViewById(R.id.TopNumber2);
        sv_TopNumber3 = (ScrollView)findViewById(R.id.TopNumber3);
        sv_TopNumber4 = (ScrollView)findViewById(R.id.TopNumber4);

        tv_txt_TodayAccount = (TextView)findViewById(R.id.txt_TodayAccount);
        tv_txt_yesterday = (TextView)findViewById(R.id.txt_yesterday);

        /*private LinearLayout lllogin, llcharge, llcontents;
        private ImageView imglogin, imgcharge, imgcontents;
        private TextView tvlogin, txtcharge, txtcontents;*/

        lllogin = (LinearLayout)findViewById(R.id.lllogin);
        llcharge = (LinearLayout)findViewById(R.id.llcharge);
        llcontents = (LinearLayout)findViewById(R.id.llcontents);
        llmyDoamin= (LinearLayout)findViewById(R.id.myDoamin);
        llmain= (LinearLayout)findViewById(R.id.llmain);

        imglogin = (ImageView)findViewById(R.id.imglogin);
        imgcharge = (ImageView)findViewById(R.id.imgcharge);
        imgcontents = (ImageView)findViewById(R.id.imgcontents);
        tvlogin = (TextView)findViewById(R.id.tvlogin);
        txtcharge = (TextView)findViewById(R.id.txtcharge);
        txtcontents = (TextView)findViewById(R.id.txtcontents);


       // llmyDoamin.setBackgroundColor(Color.parseColor("#29295c"));

        ImageView imglogin2=(ImageView)findViewById(R.id.imglogin2);
        imglogin2.setImageResource(R.drawable.icon_mydomin_fill);

        ImageView imgmain=(ImageView)findViewById(R.id.imgmain);
        imgmain.setImageResource(R.drawable.icon_main);


        llmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        lllogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                lllogin.setBackgroundColor(Color.parseColor("#29295c"));
//                llcharge.setBackgroundColor(Color.parseColor("#131632"));
//                llcontents.setBackgroundColor(Color.parseColor("#131632"));
                if (uic.status!=null && uic.status.compareTo("1") == 0) {
                    Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
                    intent.putExtra("url", "http://18673175554.gigaroom.cn:8055/gatePage?lang_type=kor&t_num=1&l_num=0&r_num=0&b_num=0&c_num=&mInfo=18673175554|186731755546ed9eyc|186731755546ed9e|186731755546ed9e390b8c02-95c6-4a8a-9244-3cd39f6987de|&move_type=myPage");
                    startActivityForResult(intent, 1000);
                }
                else
                {
                    Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
                    intent.putExtra("url", LoginURL);
                    startActivityForResult(intent, 1000);
                }
            }
        });

        llcharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (uic.status!=null && uic.status.compareTo("1") == 0) {
                    Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
                    intent.putExtra("url", "http://18673175554.gigaroom.cn:8055/gatePage?lang_type=kor&t_num=1&l_num=0&r_num=0&b_num=0&c_num=&mInfo=18673175554|186731755546ed9eyc|186731755546ed9e|186731755546ed9e390b8c02-95c6-4a8a-9244-3cd39f6987de|&move_type=cart");
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
                    intent.putExtra("url", LoginURL);
                    startActivityForResult(intent, 1000);
                }


//                lllogin.setBackgroundColor(Color.parseColor("#131632"));
//                llcharge.setBackgroundColor(Color.parseColor("#29295c"));
//                llcontents.setBackgroundColor(Color.parseColor("#131632"));


            }
        });

        llcontents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (uic.status!=null && uic.status.compareTo("1") == 0) {
                    Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
                    intent.putExtra("url", "http://18673175554.gigaroom.cn:8055/gatePage?lang_type=kor&t_num=1&l_num=0&r_num=0&b_num=0&c_num=&mInfo=18673175554|186731755546ed9eyc|186731755546ed9e|186731755546ed9e390b8c02-95c6-4a8a-9244-3cd39f6987de|&move_type=contentsMall");
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
                    intent.putExtra("url", LoginURL);
                    startActivityForResult(intent, 1000);
                }


//                lllogin.setBackgroundColor(Color.parseColor("#131632"));
//                llcharge.setBackgroundColor(Color.parseColor("#131632"));
//                llcontents.setBackgroundColor(Color.parseColor("#29295c"));

            }
        });

        llmyDoamin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(MyMainActivity.this, MyMainActivity.class);
//                startActivity(intent);
//                // finish();

            }
        });

//        imglogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                lllogin.setBackgroundColor(Color.parseColor("#29295c"));
//                llcharge.setBackgroundColor(Color.parseColor("#131632"));
//                llcontents.setBackgroundColor(Color.parseColor("#131632"));
//                Intent intent = new Intent(MainActivity.this, kfme_WebBrowser.class);
//                intent.putExtra("url", LoginURL);
//                startActivityForResult(intent, 1000);
//            }
//        });
//
//        imgcharge.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lllogin.setBackgroundColor(Color.parseColor("#131632"));
//                llcharge.setBackgroundColor(Color.parseColor("#29295c"));
//                llcontents.setBackgroundColor(Color.parseColor("#131632"));
//            }
//        });
//
//        imgcontents.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lllogin.setBackgroundColor(Color.parseColor("#131632"));
//                llcharge.setBackgroundColor(Color.parseColor("#131632"));
//                llcontents.setBackgroundColor(Color.parseColor("#29295c"));
//            }
//        });
//
//        tvlogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lllogin.setBackgroundColor(Color.parseColor("#29295c"));
//                llcharge.setBackgroundColor(Color.parseColor("#131632"));
//                llcontents.setBackgroundColor(Color.parseColor("#131632"));
//                Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
//                intent.putExtra("url", LoginURL);
//                startActivityForResult(intent, 1000);
//            }
//        });
//
//        txtcharge.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lllogin.setBackgroundColor(Color.parseColor("#131632"));
//                llcharge.setBackgroundColor(Color.parseColor("#29295c"));
//                llcontents.setBackgroundColor(Color.parseColor("#131632"));
//            }
//        });
//
//        txtcontents.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lllogin.setBackgroundColor(Color.parseColor("#29295c"));
//                llcharge.setBackgroundColor(Color.parseColor("#131632"));
//                llcontents.setBackgroundColor(Color.parseColor("#131632"));
//            }
//        });

        int resID = 0;
        String packName = "";
        String resourceStringID = "";

        for(int i = 0; i < main_totalCount; i++)
        {
            resourceStringID = "MainMenu" + Integer.toString((i+1));
            packName = this.getPackageName(); // 패키지명
            resID = getResources().getIdentifier(resourceStringID, "id", packName);
            iv_MainMenu[i] =  (ImageView)findViewById(resID);
            iv_MainMenu[i].setTag(i+1);
            iv_MainMenu[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*Message msg = mHandler.obtainMessage();
                    msg.what = 0;
                    msg.obj = v.getTag();
                    mHandler.sendMessage(msg);*/
                    Intent intent = new Intent(MyMainActivity.this, kfme_WebBrowser.class);
                    intent.putExtra("url", "http://www.naver.com");
                    startActivity(intent);
                }
            });
        }

        for(int i = 0; i < ret_TotalNumber.length; i++)
        {
            if(i == 0)
            {
                ret_TotalNumber[i] = "1";
            } else {
                ret_TotalNumber[i] = "0";
            }
        }



        //Select Button
        tv_dial_btn = (TextView)findViewById(R.id.dial_btn);
        tv_dial_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String retTotal = ret_TotalNumber[0] + ret_TotalNumber[1] + ret_TotalNumber[2] + ret_TotalNumber[3];
                Toast.makeText(getApplicationContext(), retTotal, Toast.LENGTH_LONG).show();
                /*try
                {
                    //java.net.URLEncoder.encode(new String(param.getByte("euc-kr")),"euc-kr")
                    String posttxt = URLEncoder.encode(et_searchtxt.getText().toString(),"EUC-KR");

                    et_searchtxt.getText().toString();

                    String Addr = "http://biz.epost.go.kr/KpostPortal/openapi?regkey=4e161da3b4b3c7ff81520154121695&target=postNew&query=" + posttxt;
                    URL url = new URL(Addr);
                    Log.d("postdata", "addr_search : " + Addr);
                    HttpURLConnection con = (HttpURLConnection)url.openConnection();
                    con.setRequestMethod("GET");

                    int responseCode = con.getResponseCode();
                    BufferedReader br;
                    if(responseCode == 200) {
                        br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    } else {
                        br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
                    }
                    String inputLine;
                    StringBuffer response = new StringBuffer();
                    while ((inputLine = br.readLine()) != null) {
                        response.append(inputLine);
                    }
                    br.close();
                    System.out.println(response.toString());
                } catch (Exception e) {
                    System.out.println(e);
                }*/
            }
        });

        for(int i=0; i < tb_totalCount; i++)
        {
            resourceStringID = "topH" + Integer.toString((i+1));
            packName = this.getPackageName(); // 패키지명
            resID = getResources().getIdentifier(resourceStringID, "id", packName);
            iv_topH[i] =  (ImageView)findViewById(resID);

            resourceStringID = "topCollect1_" + Integer.toString((i+1));
            resID = getResources().getIdentifier(resourceStringID, "id", packName);
            iv_topCollect[i] = (TextView)findViewById(resID);

            iv_topH[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i=0; i < iv_topH.length; i++) {
                        String resIDs = "topH" + Integer.toString((i+1));
                        if (v.getId() == getResources().getIdentifier(resIDs, "id", getApplicationContext().getPackageName()))
                        {
                            ButtonClickChangager("top." + Integer.toString((i+1)));

                            scrollToView(iv_topCollect[i], sv_TopNumber1 , 0);

                            TextView t=(TextView)findViewById(R.id.topCollect2_0);
                            scrollToView(t, sv_TopNumber2 , 0);
                            ret_TotalNumber[1]="0";

                            TextView t2=(TextView)findViewById(R.id.topCollect3_0);
                            scrollToView(t2, sv_TopNumber3 , 0);
                            ret_TotalNumber[2]="0";

                            TextView t3=(TextView)findViewById(R.id.topCollect4_0);
                            scrollToView(t3, sv_TopNumber4 , 0);
                            ret_TotalNumber[3]="0";


                            ret_TotalNumber[0] = Integer.toString(i+1);
                            Message msg = mHandler.obtainMessage();
                            msg.what = 1;
                            mHandler.sendMessage(msg);
                        }
                    }
                }
            });

            resourceStringID = "bottomH" + Integer.toString((i+1));
            packName = this.getPackageName();
            resID = getResources().getIdentifier(resourceStringID, "id", packName);
            iv_bottomH[i] = (ImageView)findViewById(resID);

            resourceStringID = "topCollect4_" + Integer.toString((i+1));
            resID = getResources().getIdentifier(resourceStringID, "id", packName);
            tv_topCollect4[i] = (TextView)findViewById(resID);

            iv_bottomH[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i = 0; i < iv_bottomH.length; i++) {
                        String resIDs = "bottomH" + Integer.toString((i+1));
                        if (v.getId() == getResources().getIdentifier(resIDs, "id", getApplicationContext().getPackageName()))
                        {
                            ButtonClickChangager("bottom." + Integer.toString((i+1)));

                            scrollToView(tv_topCollect4[i], sv_TopNumber4, 0);



                            ret_TotalNumber[3] = Integer.toString(i+1);
                            Message msg = mHandler.obtainMessage();
                            msg.what = 1;
                            mHandler.sendMessage(msg);
                        }
                    }
                }
            });
        }

        //세로 번호
        for(int i=0; i < side_totalCount; i++)
        {
            resourceStringID = "leftH" + Integer.toString((i+1));
            packName = this.getPackageName();
            resID = getResources().getIdentifier(resourceStringID, "id", packName);
            iv_leftH[i] = (ImageView)findViewById(resID);

            resourceStringID = "topCollect2_" + Integer.toString((i+1));
            resID = getResources().getIdentifier(resourceStringID, "id", packName);
            tv_topCollect2[i] = (TextView)findViewById(resID);

            iv_leftH[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i=0; i < iv_leftH.length; i++)
                    {
                        String resIDs = "leftH" + Integer.toString((i+1));
                        if (v.getId() == getResources().getIdentifier(resIDs, "id", getApplicationContext().getPackageName()))
                        {
                            ButtonClickChangager("left." + Integer.toString((i+1)));


                            TextView t2=(TextView)findViewById(R.id.topCollect3_0);
                            scrollToView(t2, sv_TopNumber3 , 0);
                            ret_TotalNumber[2]="0";

                            TextView t3=(TextView)findViewById(R.id.topCollect4_0);
                            scrollToView(t3, sv_TopNumber4 , 0);
                            ret_TotalNumber[3]="0";


                            scrollToView(tv_topCollect2[i], sv_TopNumber2 , 0);
                            ret_TotalNumber[1] = Integer.toString(i+1);
                            Message msg = mHandler.obtainMessage();
                            msg.what = 1;
                            mHandler.sendMessage(msg);
                        }
                    }
                }
            });

            resourceStringID = "rightH" + Integer.toString((i+1));
            packName = this.getPackageName();
            resID = getResources().getIdentifier(resourceStringID, "id", packName);
            iv_rightH[i] = (ImageView)findViewById(resID);

            resourceStringID = "topCollect3_" + Integer.toString((i+1));
            resID = getResources().getIdentifier(resourceStringID, "id", packName);
            tv_topCollect3[i] = (TextView)findViewById(resID);

            iv_rightH[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i=0; i < iv_rightH.length; i++)
                    {
                        String resIDs = "rightH" + Integer.toString((i+1));
                        if(v.getId() == getResources().getIdentifier(resIDs, "id", getApplicationContext().getPackageName()))
                        {
                            ButtonClickChangager("right." + Integer.toString((i+1)));

                            scrollToView(tv_topCollect3[i], sv_TopNumber3, 0);


                            TextView t3=(TextView)findViewById(R.id.topCollect4_0);
                            scrollToView(t3, sv_TopNumber4 , 0);
                            ret_TotalNumber[3]="0";

                            ret_TotalNumber[2] = Integer.toString(i+1);
                            Message msg = mHandler.obtainMessage();
                            msg.what = 1;
                            mHandler.sendMessage(msg);
                        }
                    }
                }
            });
        }

        iv_tempTop = (ImageView)findViewById(R.id.topH1);
        String ret = httpPost(ret_TotalNumber[0], ret_TotalNumber[1], ret_TotalNumber[2], ret_TotalNumber[3]);
        JsonParsingMain(ret);
        resourceStringID = "r01_p";
        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);

        BitmapDrawable img;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
        } else {
            img = (BitmapDrawable) getResources().getDrawable(resID);
        }
        iv_tempTop.setImageDrawable(img);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // 수행을 제대로 한 경우
            if (requestCode == 1000) {
                String result = data.getStringExtra("resultJson");
                try {
                    JSONObject resultJsonData = new JSONObject(result);
                    uic.status = resultJsonData.getString("status");
                    uic.flag = resultJsonData.getString("flag");
                    uic.msg = resultJsonData.getString("msg");
                    uic.ssoId = resultJsonData.getString("ssoId");
                    uic.sso_regiKey = resultJsonData.getString("sso_regiKey");
                    uic.custom_id = resultJsonData.getString("custom_id");
                    uic.custom_code = resultJsonData.getString("custom_code");
                    uic.token = resultJsonData.getString("token");
                    uic.profile_img = resultJsonData.getString("profile_img");
                    uic.process_result = resultJsonData.getString("process_result");
                    uic.result_msg = resultJsonData.getString("result_msg");
                    uic.deviceNo = resultJsonData.getString("deviceNo");
                    uic.store_code = resultJsonData.getString("store_code");
                    uic.mall_home_id = resultJsonData.getString("mall_home_id");
                    uic.message = resultJsonData.getString("message");
                    uic.memid = resultJsonData.getString("memid");
                    uic.point_card_no = resultJsonData.getString("point_card_no");
                    uic.parent_id = resultJsonData.getString("parent_id");
                    uic.parent_mall_domain = resultJsonData.getString("parent_mall_domain");
                    uic.grade = resultJsonData.getString("grade");
                    uic.grade_name = resultJsonData.getString("grade_name");
                    uic.sales_id = resultJsonData.getString("sales_id");
                    uic.minfo = resultJsonData.getString("minfo");
                    uic.visit_sub_domain = resultJsonData.getString("visit_sub_domain");
                    uic.visit_domain = resultJsonData.getString("visit_domain");
                    uic.my_sub_domain = resultJsonData.getString("my_sub_domain");
                    uic.my_domain = resultJsonData.getString("my_domain");
                    uic.my_sales_level = resultJsonData.getString("my_sales_level");
                    uic.my_nation_code = resultJsonData.getString("my_nation_code");
                    uic.my_group_code = resultJsonData.getString("my_group_code");
                    uic.my_authYN = resultJsonData.getString("my_authYN");
                    uic.my_authKey = resultJsonData.getString("my_authKey");

                    if (uic.status.compareTo("1") == 0) {
                        tvlogin.setText("LOGOUT");
                    }

                } catch (JSONException ex) {

                }
            }
            // 수행을 제대로 하지 못한 경우
            else if (requestCode == 1001) {

            }
        }catch (Exception ee){}
    }

    public static void scrollToView(View view, final ScrollView scrollView, int count) {
        if (view != null && view != scrollView) {
            count += view.getTop();
            scrollToView((View) view.getParent(), scrollView, count);
        } else if (scrollView != null) {
            final int finalCount = count;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, finalCount);
                }
            }, 200);
        }
    }

    public void MainButtonClickChangager(String btn_menu)
    {
        String packName = this.getPackageName();
        String num = "";
        String[] btn_split = btn_menu.split("\\.");
        String resourceStringID = "";

        int resID = 0;
        for(int i = 0; i < iv_MainMenu.length; i++)
        {
            if((i + 1) == Integer.parseInt(btn_split[1]))
            {
                if (Integer.toString(i+1).length() == 1)
                {
                    num = "0" + Integer.toString(i + 1);
                } else {
                    num = Integer.toString(i + 1);
                }
                resourceStringID = "n" + num + "_s";
                resID = getResources().getIdentifier(resourceStringID, "drawable", packName);

                BitmapDrawable img;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                } else {
                    img = (BitmapDrawable) getResources().getDrawable(resID);
                }
                iv_MainMenu[i].setImageDrawable(img);
            }
        }
    }

    public void ButtonClickChangager(String btn_menu)
    {
        String[] btn_split = btn_menu.split("\\.");
        if(btn_split[0].compareTo("top") == 0)
        {
            String packName = this.getPackageName();
            String num = "";
            String resourceStringID = "";

            int resID = 0;
            try {
                for (int i = 0; i < iv_topH.length; i++) {
                    if ((i + 1) == Integer.parseInt(btn_split[1])) {
                        if (Integer.toString(i+1).length() == 1) {
                            num = "0" + Integer.toString(i + 1);
                        } else {
                            num = Integer.toString(i + 1);
                        }
                        resourceStringID = "r" + num + "_p";
                        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);

                        BitmapDrawable img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                        } else {
                            img = (BitmapDrawable) getResources().getDrawable(resID);
                        }
                        iv_topH[i].setImageDrawable(img);
                    } else {
                        if (Integer.toString(i+1).length() == 1) {
                            num = "0" + Integer.toString(i + 1);
                        } else {
                            num = Integer.toString(i + 1);
                        }
                        resourceStringID = "r" + num;
                        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);
                        //iv_topH[i] = (ImageView) findViewById(resID);
                        BitmapDrawable img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                        } else {
                            img = (BitmapDrawable) getResources().getDrawable(resID);
                        }
                        iv_topH[i].setImageDrawable(img);
                    }
                }
            } catch (Exception e) {
                String error = e.toString();
            }
        } else if(btn_split[0].compareTo("bottom") == 0){
            String packName = this.getPackageName();
            String num = "";
            String resourceStringID = "";

            int resID = 0;
            try {
                for (int i = 0; i < iv_bottomH.length; i++) {
                    if ((i + 1) == Integer.parseInt(btn_split[1])) {
                        if (Integer.toString(i+1).length() == 1) {
                            num = "0" + Integer.toString(i + 1);
                        } else {
                            num = Integer.toString(i + 1);
                        }

                        resourceStringID = "b" + num + "_p";
                        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);

                        BitmapDrawable img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                        } else {
                            img = (BitmapDrawable) getResources().getDrawable(resID);
                        }
                        iv_bottomH[i].setImageDrawable(img);
                    } else {
                        if (Integer.toString(i+1).length() == 1) {
                            num = "0" + Integer.toString(i + 1);
                        } else {
                            num = Integer.toString(i + 1);
                        }

                        resourceStringID = "b" + num;
                        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);
                        //iv_topH[i] = (ImageView) findViewById(resID);
                        BitmapDrawable img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                        } else {
                            img = (BitmapDrawable) getResources().getDrawable(resID);
                        }
                        iv_bottomH[i].setImageDrawable(img);
                    }
                }
            } catch (Exception e) {
                String error = e.toString();
            }
        } else if(btn_split[0].compareTo("left") == 0) {
            String packName = this.getPackageName();
            String num = "";
            String resourceStringID = "";

            int resID = 0;
            try {
                for (int i = 0; i < iv_leftH.length; i++) {
                    if ((i + 1) == Integer.parseInt(btn_split[1])) {
                        if (Integer.toString(i+1).length() == 1) {
                            num = "0" + Integer.toString(i + 1);
                        } else {
                            num = Integer.toString(i + 1);
                        }

                        resourceStringID = "y" + num + "_p";
                        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);

                        BitmapDrawable img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                        } else {
                            img = (BitmapDrawable) getResources().getDrawable(resID);
                        }
                        iv_leftH[i].setImageDrawable(img);
                    } else {
                        if (Integer.toString(i+1).length() == 1) {
                            num = "0" + Integer.toString(i + 1);
                        } else {
                            num = Integer.toString(i + 1);
                        }

                        resourceStringID = "y" + num;
                        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);
                        //iv_topH[i] = (ImageView) findViewById(resID);
                        BitmapDrawable img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                        } else {
                            img = (BitmapDrawable) getResources().getDrawable(resID);
                        }
                        iv_leftH[i].setImageDrawable(img);
                    }
                }
            } catch (Exception e) {
                String error = e.toString();
            }
        } else if(btn_split[0].compareTo("right") == 0) {
            String packName = this.getPackageName();
            String num = "";
            String resourceStringID = "";

            int resID = 0;
            try {
                for (int i = 0; i < iv_rightH.length; i++) {
                    if ((i + 1) == Integer.parseInt(btn_split[1])) {
                        if (Integer.toString(i+1).length() == 1) {
                            num = "0" + Integer.toString(i + 1);
                        } else {
                            num = Integer.toString(i + 1);
                        }

                        resourceStringID = "g" + num + "_p";
                        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);

                        BitmapDrawable img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                        } else {
                            img = (BitmapDrawable) getResources().getDrawable(resID);
                        }
                        iv_rightH[i].setImageDrawable(img);
                    } else {
                        if (Integer.toString(i+1).length() == 1) {
                            num = "0" + Integer.toString(i + 1);
                        } else {
                            num = Integer.toString(i + 1);
                        }

                        resourceStringID = "g" + num;
                        resID = getResources().getIdentifier(resourceStringID, "drawable", packName);
                        //iv_topH[i] = (ImageView) findViewById(resID);
                        BitmapDrawable img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            img = (BitmapDrawable) getResources().getDrawable(resID, getTheme());
                        } else {
                            img = (BitmapDrawable) getResources().getDrawable(resID);
                        }
                        iv_rightH[i].setImageDrawable(img);
                    }
                }
            } catch (Exception e) {
                String error = e.toString();
            }
        }
    }

}

