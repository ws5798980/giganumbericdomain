package com.gigakorea.giganumbericdomain.Adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gigakorea.giganumbericdomain.Entity.UrlBean;
import com.gigakorea.giganumbericdomain.R;

import java.util.List;

public class BaseRecycAdapter2 extends RecyclerView.Adapter<BaseRecycAdapter2.MyHolder> {

    Context context;
    List<UrlBean.DataBean> list;
    OnItemClickListener mOnItemClickListener;

    public BaseRecycAdapter2(Context context, @Nullable List<UrlBean.DataBean> data) {
        this.context = context;
        this.list = data;
    }

    public void notifydata(List<UrlBean.DataBean> data) {
        this.list = data;
        notifyDataSetChanged();
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_main_list2, null, false);
        MyHolder myHolder = new MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(MyHolder myHolder, final int i) {
        if (i < list.size()) {
            myHolder.textView.setText(list.get(i).getAd_url());
            if (mOnItemClickListener != null) {
                myHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mOnItemClickListener.onClick(i);
                    }
                });
            }
        } else {
            myHolder.textView.setText( "no data");
        }

        myHolder.numtv.setText(i + 1 + "");
    }

    @Override
    public int getItemCount() {
        return 140;
    }


    class MyHolder extends RecyclerView.ViewHolder {
        TextView textView, numtv;

        public MyHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv);
            numtv = itemView.findViewById(R.id.tv_num);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }
}
