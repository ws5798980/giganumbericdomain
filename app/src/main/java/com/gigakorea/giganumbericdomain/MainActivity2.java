package com.gigakorea.giganumbericdomain;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gigakorea.giganumbericdomain.Adapter.BaseRecycAdapter;
import com.gigakorea.giganumbericdomain.Adapter.BaseRecycAdapter2;
import com.gigakorea.giganumbericdomain.Entity.UrlBean;
import com.rs.mobile.common.C;
import com.rs.mobile.common.L;
import com.rs.mobile.common.S;
import com.rs.mobile.common.network.OkHttpHelper;
import com.rs.mobile.common.util.GsonUtils;
import com.rs.mobile.common.util.Util;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Request;

public class MainActivity2 extends Activity {
    private List<String> data;
    private ImageView img1, img2, img3, img4, img5;
    private TextView tv1, tv2, tv3, tv4, tv5;
    private RelativeLayout tv6;
    private LinearLayout lllogin, llcharge, llcontents, llmyDoamin;
    private RecyclerView list, list2;
    private BaseRecycAdapter adapter;
    private BaseRecycAdapter2 adapter2;
    private int select;
    private UrlBean bean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());

        initView();

    }

    private void initView() {
        select = 1;
        tv1 = findViewById(R.id.tv_1);
        tv2 = findViewById(R.id.tv_2);
        tv3 = findViewById(R.id.tv_3);
        tv4 = findViewById(R.id.tv_4);
        tv5 = findViewById(R.id.tv_5);

        tv6 = findViewById(R.id.tv_6);

        img1 = findViewById(R.id.img_1);
        img2 = findViewById(R.id.img_2);
        img3 = findViewById(R.id.img_3);
        img4 = findViewById(R.id.img_4);
        img5 = findViewById(R.id.img_5);


        list = findViewById(R.id.list);
        list2 = findViewById(R.id.list2);

        lllogin = findViewById(R.id.lllogin);
        llcharge = findViewById(R.id.llcharge);
        llcontents = findViewById(R.id.llcontents);
        llmyDoamin = findViewById(R.id.myDoamin);
        data = new ArrayList<>();
        for (int i = 1; i <= 16; i++) {
            data.add(i + "");
        }

        tv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!"0".equals(tv5.getText().toString())) {
                    int position = Integer.parseInt(tv5.getText().toString());
                    if (position <= bean.getData().size()) {
                        Intent intent = new Intent(MainActivity2.this, kfme_WebBrowser.class);
                        intent.putExtra("url", bean.getData().get(position - 1).getAd_url() + "");
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity2.this, "no data", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(MainActivity2.this, "no data", Toast.LENGTH_LONG).show();
                }
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        list.setLayoutManager(linearLayoutManager);
        adapter = new BaseRecycAdapter(MainActivity2.this, data);
        adapter.setOnItemClickListener(new BaseRecycAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                switch (select) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        setdata(select, position);
                        bean = GsonUtils.changeGsonToBean(httpPost(tv1.getText().toString(), tv2.getText().toString(), tv3.getText().toString(), tv4.getText().toString()), UrlBean.class);
                        adapter2.notifydata(bean.getData());
                        break;
                    case 5:
                        setdata(select, position);
                        break;
                }


            }
        });
        list.setAdapter(adapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        list2.setLayoutManager(gridLayoutManager);
        bean = GsonUtils.changeGsonToBean(httpPost(tv1.getText().toString(), tv2.getText().toString(), tv3.getText().toString(), tv4.getText().toString()), UrlBean.class);
        adapter2 = new BaseRecycAdapter2(this, bean.getData());
        adapter2.setOnItemClickListener(new BaseRecycAdapter2.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(MainActivity2.this, kfme_WebBrowser.class);
                intent.putExtra("url", bean.getData().get(position).getAd_url() + "");
                startActivity(intent);
            }
        });
        list2.setAdapter(adapter2);

        lllogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkLogin(MainActivity2.this, "http://18673175554.gigaroom.cn:8055/gatePage?lang_type=kor&t_num=1&l_num=0&r_num=0&b_num=0&c_num=&mInfo=", "|&move_type=myPage");
            }
        });

        llcharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkLogin(MainActivity2.this, "http://18673175554.gigaroom.cn:8055/gatePage?lang_type=kor&t_num=1&l_num=0&r_num=0&b_num=0&c_num=&mInfo=", "|&move_type=cart");

            }
        });

        llcontents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkLogin(MainActivity2.this, "http://18673175554.gigaroom.cn:8055/gatePage?lang_type=kor&t_num=1&l_num=0&r_num=0&b_num=0&c_num=&mInfo=", "|&move_type=contentsMall");

            }
        });

        llmyDoamin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkLogin(MainActivity2.this, "", "");

            }
        });


        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hode(1);
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!"0".equals(tv1.getText().toString()))
                    hode(2);
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!"0".equals(tv1.getText().toString()) && !"0".equals(tv2.getText().toString()))
                    hode(3);
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!"0".equals(tv1.getText().toString()) && !"0".equals(tv2.getText().toString()) && !"0".equals(tv3.getText().toString()))
                    hode(4);
            }
        });
        tv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!"0".equals(tv1.getText().toString()) && !"0".equals(tv2.getText().toString()) && !"0".equals(tv3.getText().toString()) && !"0".equals(tv4.getText().toString()))
                    hode(5);
            }
        });


    }

    private void setdata(int select, int position) {
        switch (select) {
            case 1:
                tv1.setText(position + 1 + "");
                hode(2);
                break;
            case 2:
                tv2.setText(position + 1 + "");
                hode(3);
                break;
            case 3:
                tv3.setText(position + 1 + "");
                hode(4);
                break;
            case 4:
                tv4.setText(position + 1 + "");
                hode(5);
                break;
            case 5:
                tv5.setText(position + 1 + "");
                list2.smoothScrollToPosition(position);
                break;
        }
        switch (select) {
            case 1:
                tv2.setText("0");
            case 2:
                tv3.setText("0");
            case 3:
                tv4.setText("0");
            case 4:
                tv5.setText("0");
        }
    }


    private void hode(int i) {
        select = i;
        img1.setVisibility(View.INVISIBLE);
        img2.setVisibility(View.INVISIBLE);
        img3.setVisibility(View.INVISIBLE);
        img4.setVisibility(View.INVISIBLE);
        img5.setVisibility(View.INVISIBLE);
        data.clear();
        switch (i) {
            case 1:
                img1.setVisibility(View.VISIBLE);

                for (int j = 1; j <= 16; j++) {
                    data.add(j + "");
                }
                break;
            case 2:
                img2.setVisibility(View.VISIBLE);
                for (int j = 1; j <= 10; j++) {
                    data.add(j + "");
                }
                break;
            case 3:
                img3.setVisibility(View.VISIBLE);
                for (int j = 1; j <= 10; j++) {
                    data.add(j + "");
                }
                break;
            case 4:
                img4.setVisibility(View.VISIBLE);
                for (int j = 1; j <= 16; j++) {
                    data.add(j + "");
                }
                break;
            case 5:
                for (int j = 1; j <= 140; j++) {
                    data.add(j + "");
                }
                img5.setVisibility(View.VISIBLE);
                break;

        }

        adapter.notifydata(data);
    }


    /**
     * 检查匹配登陆（每个要求登陆接口都需要调用）
     *
     * @param context
     */
    public void checkLogin(final Context context, final String url, final String url2) {

        try {

            OkHttpHelper helper = new OkHttpHelper(context, true);

            helper.addGetRequest(new OkHttpHelper.CallbackLogic() {

                @Override
                public void onNetworkError(Request request, IOException e) {
                    String netError_result = e.toString();
                }

                @Override
                public void onBizSuccess(String responseDescription,
                                         final JSONObject data, final String flag) {

                    try {
                        if (data.getString("status").equals("1")
                                && data.getString("flag").equals("1501")) {
                            S.setShare(context, C.KEY_JSON_TOKEN,
                                    // data.getString(C.KEY_JSON_TOKEN));
                                    data.getString(C.KEY_JSON_TOKEN) + "|"
                                            + data.getString("ssoId") + "|"
                                            + data.getString("custom_code")
                                            + "|" + Util.getDeviceId(context));

                            S.set(context, C.KEY_JSON_TOKEN,
                                    // data.getString(C.KEY_JSON_TOKEN));
                                    data.getString(C.KEY_JSON_TOKEN) + "|"
                                            + data.getString("ssoId") + "|"
                                            + data.getString("custom_code")
                                            + "|" + Util.getDeviceId(context));

                            S.set(MainActivity2.this, C.KEY_JSON_CUSTOM_CODE, data.getString("custom_code"));
                            S.setShare(MainActivity2.this, C.KEY_JSON_CUSTOM_CODE, data.getString("custom_code"));

                            S.set(MainActivity2.this, C.KEY_REQUEST_MEMBER_ID, data.getString("custom_code"));
                            S.set(MainActivity2.this, C.KEY_JSON_CUSTOM_ID, data.getString("custom_id"));
                            S.set(MainActivity2.this, C.KEY_JSON_NICK_NAME, data.getString("nick_name"));
                            S.set(MainActivity2.this, C.KEY_JSON_PROFILE_IMG, data.getString("profile_img"));
                            S.set(MainActivity2.this, C.KEY_JSON_DIV_CODE, data.getString("div_code"));
                            S.set(MainActivity2.this, C.KEY_JSON_SSOID, data.getString("ssoId"));
                            S.set(MainActivity2.this, C.KEY_JSON_SSO_REGIKEY, data.getString("sso_regiKey"));
                            S.set(MainActivity2.this, C.KEY_JSON_MALL_HOME_ID, data.getString("mall_home_id"));
                            S.set(MainActivity2.this, C.KEY_JSON_POINT_CARD_NO, data.getString("point_card_no"));
                            S.set(MainActivity2.this, C.KEY_JSON_PARENT_ID, data.getString("parent_id"));

                            String allurl;
                            if (!"".equals(url)) {
                                allurl = url + S.get(MainActivity2.this, C.KEY_JSON_CUSTOM_ID, "") + "|" + S.get(MainActivity2.this, C.KEY_JSON_SSOID, "") + "|" + S.get(MainActivity2.this, C.KEY_JSON_CUSTOM_CODE, "") + "|" + S.get(MainActivity2.this, C.KEY_JSON_TOKEN, "") + url2;
                                Intent intent = new Intent(MainActivity2.this, kfme_WebBrowser.class);
                                intent.putExtra("url", allurl);

                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(MainActivity2.this, MyMainActivity.class);
                                startActivity(intent);
                            }


                        } else {

                            Intent intent = new Intent(MainActivity2.this, LoginActivity.class);
                            startActivityForResult(intent, 1000);
                        }


                    } catch (Exception e) {

                        L.e(e);

                    }

                }


                @Override
                public void onBizFailure(String responseDescription,
                                         JSONObject data, String responseCode) {
                    if (data != null) {
                        Log.d("rsapp", data.toString());
                    }
                }
            }, C.CHECK_LOGIN
                    + "?deviceNo="
                    + Util.getDeviceId(context)
                    + "&sign="
                    + encryption(Util.getDeviceId(context)
                    + "ycssologin1212121212121"));
            Log.i("xyz", C.CHECK_LOGIN
                    + "?deviceNo="
                    + Util.getDeviceId(context)
                    + "&sign="
                    + encryption(Util.getDeviceId(context)
                    + "ycssologin1212121212121"));
        } catch (Exception e) {
            e.getMessage();

        }
    }


    private String encryption(String userPassword) {

        MessageDigest md;

        String encritPassword = "";

        try {
            md = MessageDigest.getInstance("SHA-512");

            md.update(userPassword.getBytes());
            byte[] mb = md.digest();
            for (int i = 0; i < mb.length; i++) {
                byte temp = mb[i];
                String s = Integer.toHexString(new Byte(temp));
                while (s.length() < 2) {
                    s = "0" + s;
                }
                s = s.substring(s.length() - 2);
                encritPassword += s;
            }

        } catch (Exception e) {
            String ecryption_error = e.toString();
        }

        return encritPassword;
    }


    public String httpPost(String topNumber, String leftNumber, String rightNumber, String bottomNumber) {
        String retvalue = "";
        try {
            //java.net.URLEncoder.encode(new String(param.getByte("euc-kr")),"euc-kr")

            String Addr = "http://apiAD.gigaroom.cn:9280/api/apiSpecialAD/requestMainInfo";
            URL url = new URL(Addr);
            //String param = "lang_type=kor&memid=&token=t_num=" + topNumber + "&l_num=" + leftNumber + "&r_num=" + rightNumber + "&b_num=" + bottomNumber + "&visit_sub_domain=www&visit_domain=gigaroom.cn";

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setReadTimeout(5 * 1000);
            httpURLConnection.setRequestProperty("Cache_Control", "no-cache");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);

            String json_Request_Data = "{lang_type:'kor',memid:'',token:'',t_num:'" + topNumber + "',l_num:'" + leftNumber + "',r_num:'" + rightNumber + "',b_num:'" + bottomNumber + "',visit_sub_domain:'www',visit_domain:'gigaroom.cn'}";

            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(json_Request_Data.getBytes());
            outputStream.flush();
            outputStream.close();
            httpURLConnection.connect();

            String temp = null;

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            while ((temp = bufferedReader.readLine()) != null) {
                retvalue += temp;
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return retvalue;
    }
}

