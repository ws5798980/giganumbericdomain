package com.gigakorea.giganumbericdomain.Entity;

import java.util.List;

public class UrlBean {
    /**
     * data : [{"seq_no":"3268","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"58","ad_num":"1-0-0-0-58","ad_url":"https://www.colgate.com"},{"seq_no":"3269","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"59","ad_num":"1-0-0-0-59","ad_url":"https://www.chloe.cn/cn"},{"seq_no":"3270","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"60","ad_num":"1-0-0-0-60","ad_url":"https://www.starbucks.com/"},{"seq_no":"3271","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"61","ad_num":"1-0-0-0-61","ad_url":"https://www.michaelkors.cn"},{"seq_no":"3272","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"62","ad_num":"1-0-0-0-62","ad_url":"https://www.mastercard.com"},{"seq_no":"3273","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"63","ad_num":"1-0-0-0-63","ad_url":"http://www.miumiu.com/"},{"seq_no":"3274","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"64","ad_num":"1-0-0-0-64","ad_url":"https://www.3m.com"},{"seq_no":"3275","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"65","ad_num":"1-0-0-0-65","ad_url":"http://www.givenchybeauty.cn/"},{"seq_no":"3276","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"66","ad_num":"1-0-0-0-66","ad_url":"http://www.morganstanley.com/"},{"seq_no":"3277","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"67","ad_num":"1-0-0-0-67","ad_url":"https://www.jomalone.com.cn/"},{"seq_no":"3278","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"68","ad_num":"1-0-0-0-68","ad_url":"http://www.discovery.com"},{"seq_no":"3279","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"69","ad_num":"1-0-0-0-69","ad_url":"https://www.guerlain.com.cn/"},{"seq_no":"3280","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"70","ad_num":"1-0-0-0-70","ad_url":"https://www.adobe.com"},{"seq_no":"3281","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"71","ad_num":"1-0-0-0-71","ad_url":"https://www.fujixerox.com"},{"seq_no":"3282","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"72","ad_num":"1-0-0-0-72","ad_url":"https://www.kfc.com/?georedirect=false"},{"seq_no":"3283","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"73","ad_num":"1-0-0-0-73","ad_url":"http://www.tsinghua.edu.cn/"},{"seq_no":"3284","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"74","ad_num":"1-0-0-0-74","ad_url":"https://www.ikea.com"},{"seq_no":"3285","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"75","ad_num":"1-0-0-0-75","ad_url":"http://www.dhl.com"},{"seq_no":"3286","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"76","ad_num":"1-0-0-0-76","ad_url":"https://www.harvard.edu/"},{"seq_no":"3287","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"77","ad_num":"1-0-0-0-77","ad_url":"http://tv.cctv.com/"},{"seq_no":"3288","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"78","ad_num":"1-0-0-0-78","ad_url":"http://www.10086.cn/"},{"seq_no":"3289","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"79","ad_num":"1-0-0-0-79","ad_url":"https://www.hugoboss.com"},{"seq_no":"3290","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"80","ad_num":"1-0-0-0-80","ad_url":"http://www.icbc.com.cn/icbc/"},{"seq_no":"3291","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"81","ad_num":"1-0-0-0-81","ad_url":"http://www.chevrolet.com/"},{"seq_no":"3292","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"82","ad_num":"1-0-0-0-82","ad_url":"https://www.mercedes-benz.com"},{"seq_no":"3293","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"83","ad_num":"1-0-0-0-83","ad_url":"http://www.zju.edu.cn/"},{"seq_no":"3294","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"84","ad_num":"1-0-0-0-84","ad_url":"https://www.usc.edu/"},{"seq_no":"3295","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"85","ad_num":"1-0-0-0-85","ad_url":"http://www.facebook.com/"},{"seq_no":"3296","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"86","ad_num":"1-0-0-0-86","ad_url":"https://www.paypal.com"},{"seq_no":"3297","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"87","ad_num":"1-0-0-0-87","ad_url":"https://www.sap.com/index.html"},{"seq_no":"3298","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"88","ad_num":"1-0-0-0-88","ad_url":"https://www.alipay.com/"},{"seq_no":"3299","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"89","ad_num":"1-0-0-0-89","ad_url":"https://www.volkswagen.com/"},{"seq_no":"3300","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"90","ad_num":"1-0-0-0-90","ad_url":"https://weixin.qq.com/"},{"seq_no":"3301","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"91","ad_num":"1-0-0-0-91","ad_url":"https://www.landrover.com"},{"seq_no":"3302","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"92","ad_num":"1-0-0-0-92","ad_url":"http://www.youtube.com/"},{"seq_no":"3303","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"93","ad_num":"1-0-0-0-93","ad_url":"https://www.porsche.com/"},{"seq_no":"3304","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"94","ad_num":"1-0-0-0-94","ad_url":"http://www.twitter.com/"},{"seq_no":"3305","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"95","ad_num":"1-0-0-0-95","ad_url":"https://www.hm.com/"},{"seq_no":"3306","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"96","ad_num":"1-0-0-0-96","ad_url":"http://linecorp.com/"},{"seq_no":"3307","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"97","ad_num":"1-0-0-0-97","ad_url":"https://www.mini.com"},{"seq_no":"3308","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"98","ad_num":"1-0-0-0-98","ad_url":"https://www.jd.com"},{"seq_no":"3309","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"99","ad_num":"1-0-0-0-99","ad_url":"https://www.deere.com"},{"seq_no":"3310","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"100","ad_num":"1-0-0-0-100","ad_url":"https://618.tmall.com"},{"seq_no":"3311","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"101","ad_num":"1-0-0-0-101","ad_url":"https://www.philips.com"},{"seq_no":"3312","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"102","ad_num":"1-0-0-0-102","ad_url":"https://www.jnj.com/"},{"seq_no":"3313","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"103","ad_num":"1-0-0-0-103","ad_url":"http://www.americanbenefitlife.com/"},{"seq_no":"3314","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"104","ad_num":"1-0-0-0-104","ad_url":"http://www.gmarket.co.kr/"},{"seq_no":"3315","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"105","ad_num":"1-0-0-0-105","ad_url":"https://www.axa.com/"},{"seq_no":"3316","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"106","ad_num":"1-0-0-0-106","ad_url":"http://www.nescafe.com"},{"seq_no":"3317","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"107","ad_num":"1-0-0-0-107","ad_url":"http://www.henricartierbresson.org/"},{"seq_no":"3318","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"108","ad_num":"1-0-0-0-108","ad_url":"http://www.cartier.com/"},{"seq_no":"3319","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"109","ad_num":"1-0-0-0-109","ad_url":"http://www.zara.com"},{"seq_no":"3320","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"110","ad_num":"1-0-0-0-110","ad_url":"http://www.mtv.com/"},{"seq_no":"3321","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"111","ad_num":"1-0-0-0-111","ad_url":"https://www.lenovo.com"},{"seq_no":"3322","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"112","ad_num":"1-0-0-0-112","ad_url":"https://www.panasonic.com"},{"seq_no":"3323","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"113","ad_num":"1-0-0-0-113","ad_url":"https://www.bmw.com"},{"seq_no":"3324","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"114","ad_num":"1-0-0-0-114","ad_url":"https://www.mi.com"},{"seq_no":"3325","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"115","ad_num":"1-0-0-0-115","ad_url":"https://www.prada.com"},{"seq_no":"3326","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"116","ad_num":"1-0-0-0-116","ad_url":"http://www.canon.com"},{"seq_no":"3327","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"117","ad_num":"1-0-0-0-117","ad_url":"http://www.hermes.com"},{"seq_no":"3328","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"118","ad_num":"1-0-0-0-118","ad_url":"http://www.sk.com/"},{"seq_no":"3329","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"119","ad_num":"1-0-0-0-119","ad_url":"https://www.360.com"},{"seq_no":"3330","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"120","ad_num":"1-0-0-0-120","ad_url":"https://www.kleenex.com"},{"seq_no":"3331","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"121","ad_num":"1-0-0-0-121","ad_url":"https://www.tesla.com/"},{"seq_no":"3332","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"122","ad_num":"1-0-0-0-122","ad_url":"http://www.huawei.com"},{"seq_no":"3333","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"123","ad_num":"1-0-0-0-123","ad_url":"http://www.chanel.com"},{"seq_no":"3334","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"124","ad_num":"1-0-0-0-124","ad_url":"http://www1.ap.dell.com"},{"seq_no":"3335","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"125","ad_num":"1-0-0-0-125","ad_url":"https://www.lego.com"},{"seq_no":"3336","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"126","ad_num":"1-0-0-0-126","ad_url":"https://www.lamborghini.com/"},{"seq_no":"3337","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"127","ad_num":"1-0-0-0-127","ad_url":"http://www.lge.com"},{"seq_no":"3338","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"128","ad_num":"1-0-0-0-128","ad_url":"http://corporate.exxonmobil.com/"},{"seq_no":"3339","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"129","ad_num":"1-0-0-0-129","ad_url":"https://www.yahoo.com/"},{"seq_no":"3340","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"130","ad_num":"1-0-0-0-130","ad_url":"http://www.spacex.com/"},{"seq_no":"3341","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"131","ad_num":"1-0-0-0-131","ad_url":"https://www.antfin.com"},{"seq_no":"3342","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"132","ad_num":"1-0-0-0-132","ad_url":"https://www.forbes.com/"},{"seq_no":"3343","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"133","ad_num":"1-0-0-0-133","ad_url":"https://www.tencent.com"},{"seq_no":"3344","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"134","ad_num":"1-0-0-0-134","ad_url":"http://www.wanda.cn/"},{"seq_no":"3346","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"136","ad_num":"1-0-0-0-136","ad_url":"http://www.kochind.com/"},{"seq_no":"3347","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"137","ad_num":"1-0-0-0-137","ad_url":"https://www.forbes.com/"},{"seq_no":"3348","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"138","ad_num":"1-0-0-0-138","ad_url":"https://www.bloomberg.com/asia"},{"seq_no":"3345","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"135","ad_num":"1-0-0-0-135","ad_url":"http://www.berkshirehathaway.com/"},{"seq_no":"3349","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"139","ad_num":"1-0-0-0-139","ad_url":"https://www.daum.net/"},{"seq_no":"3350","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"140","ad_num":"1-0-0-0-140","ad_url":"https://www.lvmh.com/"},{"seq_no":"3226","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"16","ad_num":"1-0-0-0-16","ad_url":"https://www.sony.com"},{"seq_no":"3227","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"17","ad_num":"1-0-0-0-17","ad_url":"https://www.louisvuitton.cn/"},{"seq_no":"3228","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"18","ad_num":"1-0-0-0-18","ad_url":"http://www.disney.com"},{"seq_no":"3229","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"19","ad_num":"1-0-0-0-19","ad_url":"https://www.chanel.cn/zh_CN/"},{"seq_no":"3230","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"20","ad_num":"1-0-0-0-20","ad_url":"https://www.loreal.com/"},{"seq_no":"3231","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"21","ad_num":"1-0-0-0-21","ad_url":"https://cn.burberry.com/"},{"seq_no":"3232","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"22","ad_num":"1-0-0-0-22","ad_url":"https://www.audi.com"},{"seq_no":"3233","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"23","ad_num":"1-0-0-0-23","ad_url":"http://www.hermes.cn/index_cn.html"},{"seq_no":"3234","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"24","ad_num":"1-0-0-0-24","ad_url":"http://www.kia.com/"},{"seq_no":"3235","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"25","ad_num":"1-0-0-0-25","ad_url":"https://www.dior.cn/home/zh_cn"},{"seq_no":"3236","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"26","ad_num":"1-0-0-0-26","ad_url":"https://www.siemens.com/"},{"seq_no":"3237","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"27","ad_num":"1-0-0-0-27","ad_url":"http://www.lacoste.com.cn/"},{"seq_no":"3238","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"28","ad_num":"1-0-0-0-28","ad_url":"https://www.toyota.com"},{"seq_no":"3239","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"29","ad_num":"1-0-0-0-29","ad_url":"http://www.izzue.com/"},{"seq_no":"3240","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"30","ad_num":"1-0-0-0-30","ad_url":"https://www.oracle.com"},{"seq_no":"3241","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"31","ad_num":"1-0-0-0-31","ad_url":"http://www.uniqlo.cn/"},{"seq_no":"3242","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"32","ad_num":"1-0-0-0-32","ad_url":"https://www.burberry.com/"},{"seq_no":"3243","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"33","ad_num":"1-0-0-0-33","ad_url":"http://www.lg.com/cn"},{"seq_no":"3244","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"34","ad_num":"1-0-0-0-34","ad_url":"https://www.nike.com"},{"seq_no":"3245","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"35","ad_num":"1-0-0-0-35","ad_url":"https://www.zte.com.cn/global/"},{"seq_no":"3246","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"36","ad_num":"1-0-0-0-36","ad_url":"https://www.cisco.com"},{"seq_no":"3247","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"37","ad_num":"1-0-0-0-37","ad_url":"http://www.huawei.com/cn/"},{"seq_no":"3248","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"38","ad_num":"1-0-0-0-38","ad_url":"https://www.hpe.com"},{"seq_no":"3249","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"39","ad_num":"1-0-0-0-39","ad_url":"http://www.hisense.com/"},{"seq_no":"3250","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"40","ad_num":"1-0-0-0-40","ad_url":"http://www.pepsico.com/"},{"seq_no":"3251","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"41","ad_num":"1-0-0-0-41","ad_url":"https://www.omegawatches.cn/cn/"},{"seq_no":"3252","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"42","ad_num":"1-0-0-0-42","ad_url":"https://www.americanexpress.com"},{"seq_no":"3253","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"43","ad_num":"1-0-0-0-43","ad_url":"http://www.rolex.com"},{"seq_no":"3254","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"44","ad_num":"1-0-0-0-44","ad_url":"https://www.ebay.com/"},{"seq_no":"3255","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"45","ad_num":"1-0-0-0-45","ad_url":"http://www.cartier.cn/"},{"seq_no":"3256","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"46","ad_num":"1-0-0-0-46","ad_url":"http://www.jpmorgan.com"},{"seq_no":"3257","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"47","ad_num":"1-0-0-0-47","ad_url":"https://www.tiffany.cn/"},{"seq_no":"3258","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"48","ad_num":"1-0-0-0-48","ad_url":"https://www.kelloggs.com/en_US/home.html"},{"seq_no":"3259","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"49","ad_num":"1-0-0-0-49","ad_url":"https://cn.mcmworldwide.com/"},{"seq_no":"3260","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"50","ad_num":"1-0-0-0-50","ad_url":"https://www.nissanusa.com/"},{"seq_no":"3261","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"51","ad_num":"1-0-0-0-51","ad_url":"https://cn.mikimoto.com/sc/"},{"seq_no":"3262","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"52","ad_num":"1-0-0-0-52","ad_url":"https://www.citigroup.com/citi/"},{"seq_no":"3263","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"53","ad_num":"1-0-0-0-53","ad_url":"http://www.fendi.cn/"},{"seq_no":"3264","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"54","ad_num":"1-0-0-0-54","ad_url":"http://www.goldmansachs.com/"},{"seq_no":"3265","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"55","ad_num":"1-0-0-0-55","ad_url":"https://www.versace.cn/zh-cn/"},{"seq_no":"3266","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"56","ad_num":"1-0-0-0-56","ad_url":"http://www.budweiser.com"},{"seq_no":"3267","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"57","ad_num":"1-0-0-0-57","ad_url":"https://www.gucci.cn"},{"seq_no":"3134","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"77","ad_num":"1-0-0-0-77","ad_url":""},{"seq_no":"3211","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"1","ad_num":"1-0-0-0-1","ad_url":"http://www.samsung.com/cn/"},{"seq_no":"3212","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"2","ad_num":"1-0-0-0-2","ad_url":"http://www.citigroup.com/"},{"seq_no":"3213","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"3","ad_num":"1-0-0-0-3","ad_url":"http://www.hsbc.com/"},{"seq_no":"3214","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"4","ad_num":"1-0-0-0-4","ad_url":"http://www.apple.com"},{"seq_no":"3215","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"5","ad_num":"1-0-0-0-5","ad_url":"https://www.microsoft.com/"},{"seq_no":"3216","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"6","ad_num":"1-0-0-0-6","ad_url":"http://www.amazon.com"},{"seq_no":"3217","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"7","ad_num":"1-0-0-0-7","ad_url":"https://www.baidu.com/"},{"seq_no":"3218","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"8","ad_num":"1-0-0-0-8","ad_url":"https://www.intel.com"},{"seq_no":"3219","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"9","ad_num":"1-0-0-0-9","ad_url":"http://www.ibm.com/"},{"seq_no":"3220","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"10","ad_num":"1-0-0-0-10","ad_url":"https://www.coca-cola.com/global/"},{"seq_no":"3221","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"11","ad_num":"1-0-0-0-11","ad_url":"https://www.apple.com/"},{"seq_no":"3222","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"12","ad_num":"1-0-0-0-12","ad_url":"https://www.taobao.com/"},{"seq_no":"3223","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"13","ad_num":"1-0-0-0-13","ad_url":"https://www.skii.com.cn/"},{"seq_no":"3224","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"14","ad_num":"1-0-0-0-14","ad_url":"http://worldwide.hyundai.com/"},{"seq_no":"3225","t_num":"1","l_num":"0","r_num":"0","b_num":"0","c_num":"15","ad_num":"1-0-0-0-15","ad_url":"http://www.lancome.com.cn/"}]
     * info : {"today_mem_count":"0","yesterday_mem_count":"0","tot_mem_count":"646","ad_ing_count":"1435","tot_point":"0"}
     * numAuth : {"custom_code":"13266719901cebe1","sales_level":"1100","nation_code":"82","group_code":"-1","area_level1":"-1","area_level2":"-1","area_level3":"-1        ","sub_domain":"13266719901","domain":"gigaroom.cn","sales_parent_id":null,"s_date":null,"e_date":null,"isDelete":null}
     * cate : [{"lang_type":"kor","sort_seq":"1","custom_code":"13266719901cebe1","position":"t","menu_num":"1","menu_name":"GIGA","menu_image":null,"contents_key":"","editYN":"N"},{"lang_type":"kor","sort_seq":"2","custom_code":"13266719901cebe1","position":"t","menu_num":"2","menu_name":"뷰티","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"3","custom_code":"13266719901cebe1","position":"t","menu_num":"3","menu_name":"유아동","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"4","custom_code":"13266719901cebe1","position":"t","menu_num":"4","menu_name":"식품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"5","custom_code":"13266719901cebe1","position":"t","menu_num":"5","menu_name":"가구","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"6","custom_code":"13266719901cebe1","position":"t","menu_num":"6","menu_name":"리빙","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"7","custom_code":"13266719901cebe1","position":"t","menu_num":"7","menu_name":"건강","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"8","custom_code":"13266719901cebe1","position":"t","menu_num":"8","menu_name":"스포츠","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"9","custom_code":"13266719901cebe1","position":"t","menu_num":"9","menu_name":"자동차","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"10","custom_code":"13266719901cebe1","position":"t","menu_num":"10","menu_name":"취미","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"11","custom_code":"13266719901cebe1","position":"t","menu_num":"11","menu_name":"컴퓨터","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"12","custom_code":"13266719901cebe1","position":"t","menu_num":"12","menu_name":"디지털","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"13","custom_code":"13266719901cebe1","position":"t","menu_num":"13","menu_name":"가전","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"14","custom_code":"13266719901cebe1","position":"t","menu_num":"14","menu_name":"도서","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"15","custom_code":"13266719901cebe1","position":"t","menu_num":"15","menu_name":"티켓","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"16","custom_code":"13266719901cebe1","position":"t","menu_num":"16","menu_name":"여행","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"1","custom_code":"13266719901cebe1","position":"l","menu_num":"1","menu_name":"패션의류","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"2","custom_code":"13266719901cebe1","position":"l","menu_num":"2","menu_name":"패션잡화","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"3","custom_code":"13266719901cebe1","position":"l","menu_num":"3","menu_name":"화장품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"4","custom_code":"13266719901cebe1","position":"l","menu_num":"4","menu_name":"유아용품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"5","custom_code":"13266719901cebe1","position":"l","menu_num":"5","menu_name":"농수축산물","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"6","custom_code":"13266719901cebe1","position":"l","menu_num":"6","menu_name":"침구","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"7","custom_code":"13266719901cebe1","position":"l","menu_num":"7","menu_name":"실버용품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"8","custom_code":"13266719901cebe1","position":"l","menu_num":"8","menu_name":"아웃도어","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"9","custom_code":"13266719901cebe1","position":"l","menu_num":"9","menu_name":"자동차용품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"10","custom_code":"13266719901cebe1","position":"l","menu_num":"10","menu_name":"산업용품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"1","custom_code":"13266719901cebe1","position":"r","menu_num":"1","menu_name":"모바일","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"2","custom_code":"13266719901cebe1","position":"r","menu_num":"2","menu_name":"수입명품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"3","custom_code":"13266719901cebe1","position":"r","menu_num":"3","menu_name":"조명","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"4","custom_code":"13266719901cebe1","position":"r","menu_num":"4","menu_name":"사무용품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"5","custom_code":"13266719901cebe1","position":"r","menu_num":"5","menu_name":"꽃/원예","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"6","custom_code":"13266719901cebe1","position":"r","menu_num":"6","menu_name":"컴퓨터부품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"7","custom_code":"13266719901cebe1","position":"r","menu_num":"7","menu_name":"건강기기","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"8","custom_code":"13266719901cebe1","position":"r","menu_num":"8","menu_name":"자전거","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"9","custom_code":"13266719901cebe1","position":"r","menu_num":"9","menu_name":"캠핑","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"10","custom_code":"13266719901cebe1","position":"r","menu_num":"10","menu_name":"e쿠폰","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"1","custom_code":"13266719901cebe1","position":"b","menu_num":"1","menu_name":"소호샵","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"2","custom_code":"13266719901cebe1","position":"b","menu_num":"2","menu_name":"해외직구","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"3","custom_code":"13266719901cebe1","position":"b","menu_num":"3","menu_name":"뷰티로드","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"4","custom_code":"13266719901cebe1","position":"b","menu_num":"4","menu_name":"보험상품","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"5","custom_code":"13266719901cebe1","position":"b","menu_num":"5","menu_name":"클릭닉","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"6","custom_code":"13266719901cebe1","position":"b","menu_num":"6","menu_name":"기프티콘","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"7","custom_code":"13266719901cebe1","position":"b","menu_num":"7","menu_name":"브랜드","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"8","custom_code":"13266719901cebe1","position":"b","menu_num":"8","menu_name":"특산물","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"9","custom_code":"13266719901cebe1","position":"b","menu_num":"9","menu_name":"주얼리","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"10","custom_code":"13266719901cebe1","position":"b","menu_num":"10","menu_name":"수입가구","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"11","custom_code":"13266719901cebe1","position":"b","menu_num":"11","menu_name":"특가마켓","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"12","custom_code":"13266719901cebe1","position":"b","menu_num":"12","menu_name":"포인트샵","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"13","custom_code":"13266719901cebe1","position":"b","menu_num":"13","menu_name":"키즈클럽","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"14","custom_code":"13266719901cebe1","position":"b","menu_num":"14","menu_name":"선물","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"15","custom_code":"13266719901cebe1","position":"b","menu_num":"15","menu_name":"백화점","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"16","custom_code":"13266719901cebe1","position":"b","menu_num":"16","menu_name":"중고마켓","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"3","custom_code":"13266719901cebe1","position":"tl","menu_num":"1","menu_name":"다총판","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"4","custom_code":"13266719901cebe1","position":"tr","menu_num":"2","menu_name":"회원가입","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"1","custom_code":"13266719901cebe1","position":"bl","menu_num":"3","menu_name":"단상추","menu_image":null,"contents_key":"","editYN":""},{"lang_type":"kor","sort_seq":"2","custom_code":"13266719901cebe1","position":"br","menu_num":"4","menu_name":"마이페이지","menu_image":null,"contents_key":"","editYN":""}]
     * status : 1
     * flag : 1502
     * msg : 성공
     */

    private InfoBean info;
    private NumAuthBean numAuth;
    private String status;
    private String flag;
    private String msg;
    private List<DataBean> data;
    private List<CateBean> cate;

    public InfoBean getInfo() {
        return info;
    }

    public void setInfo(InfoBean info) {
        this.info = info;
    }

    public NumAuthBean getNumAuth() {
        return numAuth;
    }

    public void setNumAuth(NumAuthBean numAuth) {
        this.numAuth = numAuth;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public List<CateBean> getCate() {
        return cate;
    }

    public void setCate(List<CateBean> cate) {
        this.cate = cate;
    }

    public static class InfoBean {
        /**
         * today_mem_count : 0
         * yesterday_mem_count : 0
         * tot_mem_count : 646
         * ad_ing_count : 1435
         * tot_point : 0
         */

        private String today_mem_count;
        private String yesterday_mem_count;
        private String tot_mem_count;
        private String ad_ing_count;
        private String tot_point;

        public String getToday_mem_count() {
            return today_mem_count;
        }

        public void setToday_mem_count(String today_mem_count) {
            this.today_mem_count = today_mem_count;
        }

        public String getYesterday_mem_count() {
            return yesterday_mem_count;
        }

        public void setYesterday_mem_count(String yesterday_mem_count) {
            this.yesterday_mem_count = yesterday_mem_count;
        }

        public String getTot_mem_count() {
            return tot_mem_count;
        }

        public void setTot_mem_count(String tot_mem_count) {
            this.tot_mem_count = tot_mem_count;
        }

        public String getAd_ing_count() {
            return ad_ing_count;
        }

        public void setAd_ing_count(String ad_ing_count) {
            this.ad_ing_count = ad_ing_count;
        }

        public String getTot_point() {
            return tot_point;
        }

        public void setTot_point(String tot_point) {
            this.tot_point = tot_point;
        }
    }

    public static class NumAuthBean {
        /**
         * custom_code : 13266719901cebe1
         * sales_level : 1100
         * nation_code : 82
         * group_code : -1
         * area_level1 : -1
         * area_level2 : -1
         * area_level3 : -1
         * sub_domain : 13266719901
         * domain : gigaroom.cn
         * sales_parent_id : null
         * s_date : null
         * e_date : null
         * isDelete : null
         */

        private String custom_code;
        private String sales_level;
        private String nation_code;
        private String group_code;
        private String area_level1;
        private String area_level2;
        private String area_level3;
        private String sub_domain;
        private String domain;
        private Object sales_parent_id;
        private Object s_date;
        private Object e_date;
        private Object isDelete;

        public String getCustom_code() {
            return custom_code;
        }

        public void setCustom_code(String custom_code) {
            this.custom_code = custom_code;
        }

        public String getSales_level() {
            return sales_level;
        }

        public void setSales_level(String sales_level) {
            this.sales_level = sales_level;
        }

        public String getNation_code() {
            return nation_code;
        }

        public void setNation_code(String nation_code) {
            this.nation_code = nation_code;
        }

        public String getGroup_code() {
            return group_code;
        }

        public void setGroup_code(String group_code) {
            this.group_code = group_code;
        }

        public String getArea_level1() {
            return area_level1;
        }

        public void setArea_level1(String area_level1) {
            this.area_level1 = area_level1;
        }

        public String getArea_level2() {
            return area_level2;
        }

        public void setArea_level2(String area_level2) {
            this.area_level2 = area_level2;
        }

        public String getArea_level3() {
            return area_level3;
        }

        public void setArea_level3(String area_level3) {
            this.area_level3 = area_level3;
        }

        public String getSub_domain() {
            return sub_domain;
        }

        public void setSub_domain(String sub_domain) {
            this.sub_domain = sub_domain;
        }

        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public Object getSales_parent_id() {
            return sales_parent_id;
        }

        public void setSales_parent_id(Object sales_parent_id) {
            this.sales_parent_id = sales_parent_id;
        }

        public Object getS_date() {
            return s_date;
        }

        public void setS_date(Object s_date) {
            this.s_date = s_date;
        }

        public Object getE_date() {
            return e_date;
        }

        public void setE_date(Object e_date) {
            this.e_date = e_date;
        }

        public Object getIsDelete() {
            return isDelete;
        }

        public void setIsDelete(Object isDelete) {
            this.isDelete = isDelete;
        }
    }

    public static class DataBean {
        /**
         * seq_no : 3268
         * t_num : 1
         * l_num : 0
         * r_num : 0
         * b_num : 0
         * c_num : 58
         * ad_num : 1-0-0-0-58
         * ad_url : https://www.colgate.com
         */

        private String seq_no;
        private String t_num;
        private String l_num;
        private String r_num;
        private String b_num;
        private String c_num;
        private String ad_num;
        private String ad_url;

        public String getSeq_no() {
            return seq_no;
        }

        public void setSeq_no(String seq_no) {
            this.seq_no = seq_no;
        }

        public String getT_num() {
            return t_num;
        }

        public void setT_num(String t_num) {
            this.t_num = t_num;
        }

        public String getL_num() {
            return l_num;
        }

        public void setL_num(String l_num) {
            this.l_num = l_num;
        }

        public String getR_num() {
            return r_num;
        }

        public void setR_num(String r_num) {
            this.r_num = r_num;
        }

        public String getB_num() {
            return b_num;
        }

        public void setB_num(String b_num) {
            this.b_num = b_num;
        }

        public String getC_num() {
            return c_num;
        }

        public void setC_num(String c_num) {
            this.c_num = c_num;
        }

        public String getAd_num() {
            return ad_num;
        }

        public void setAd_num(String ad_num) {
            this.ad_num = ad_num;
        }

        public String getAd_url() {
            return ad_url;
        }

        public void setAd_url(String ad_url) {
            this.ad_url = ad_url;
        }
    }

    public static class CateBean {
        /**
         * lang_type : kor
         * sort_seq : 1
         * custom_code : 13266719901cebe1
         * position : t
         * menu_num : 1
         * menu_name : GIGA
         * menu_image : null
         * contents_key :
         * editYN : N
         */

        private String lang_type;
        private String sort_seq;
        private String custom_code;
        private String position;
        private String menu_num;
        private String menu_name;
        private Object menu_image;
        private String contents_key;
        private String editYN;

        public String getLang_type() {
            return lang_type;
        }

        public void setLang_type(String lang_type) {
            this.lang_type = lang_type;
        }

        public String getSort_seq() {
            return sort_seq;
        }

        public void setSort_seq(String sort_seq) {
            this.sort_seq = sort_seq;
        }

        public String getCustom_code() {
            return custom_code;
        }

        public void setCustom_code(String custom_code) {
            this.custom_code = custom_code;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getMenu_num() {
            return menu_num;
        }

        public void setMenu_num(String menu_num) {
            this.menu_num = menu_num;
        }

        public String getMenu_name() {
            return menu_name;
        }

        public void setMenu_name(String menu_name) {
            this.menu_name = menu_name;
        }

        public Object getMenu_image() {
            return menu_image;
        }

        public void setMenu_image(Object menu_image) {
            this.menu_image = menu_image;
        }

        public String getContents_key() {
            return contents_key;
        }

        public void setContents_key(String contents_key) {
            this.contents_key = contents_key;
        }

        public String getEditYN() {
            return editYN;
        }

        public void setEditYN(String editYN) {
            this.editYN = editYN;
        }
    }
}
