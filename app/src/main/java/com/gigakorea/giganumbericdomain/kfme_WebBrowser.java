package com.gigakorea.giganumbericdomain;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rs.mobile.common.AppConfig;
import com.rs.mobile.common.C;
import com.rs.mobile.common.L;
import com.rs.mobile.common.S;
import com.rs.mobile.common.network.OkHttpHelper;
import com.rs.mobile.common.util.Util;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Request;

public class kfme_WebBrowser extends Activity {

    private String urls = "";
    private WebView wv_webActive;
    private TextView tv_closebtn;
    private LinearLayout iv_back;

    public Handler handler = new Handler();

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kfme__web_browser);

        wv_webActive = (WebView) findViewById(R.id.webActive);
        iv_back = (LinearLayout) findViewById(R.id.iv_back);


        Intent intent = getIntent();
        urls = intent.getExtras().getString("url");

        //Toast.makeText(getApplicationContext(), urls, Toast.LENGTH_SHORT).show();

        WebSettings set = wv_webActive.getSettings();
        set.setJavaScriptEnabled(true);
        set.setBuiltInZoomControls(true);

        set.setJavaScriptCanOpenWindowsAutomatically(true);
        set.setAllowFileAccess(true);// 设置允许访问文件数据
        set.setSupportZoom(true);
        set.setDomStorageEnabled(true);
        set.setDatabaseEnabled(true);

        wv_webActive.addJavascriptInterface(new WebControlBridge(), "GigaWebBridge");
        wv_webActive.setWebViewClient(new WebViewClient());
        wv_webActive.setWebChromeClient(new WebChromeClient());
//        wv_webActive.setWebViewClient(new WebViewClient() {
//            public void onPageFinished(WebView view, String url) {
//
//            }
//        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (wv_webActive.canGoBack()) {
                    wv_webActive.goBack();
                } else {
                    finish();
                }
            }
        });
        wv_webActive.loadUrl(urls);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    final class WebControlBridge {
        @android.webkit.JavascriptInterface
        public void setLogin(final String userInfo) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d("jsonData", userInfo);
                    Intent result = new Intent();
                    result.putExtra("resultJson", userInfo);
                    setResult(RESULT_OK, result);
                    finish();
                }
            });
        }

        @android.webkit.JavascriptInterface
        public void logout() {
            handler.post(new Runnable() {
                @Override
                public void run() {

                    logout2();


                }
            });
        }
    }

    private void logout2() {


        JSONObject obj = new JSONObject();
        try {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json;Charset=UTF-8");

            obj.put("lang_type", AppConfig.LANG_TYPE);
            obj.put("id", S.get(this, C.KEY_JSON_CUSTOM_CODE));
            obj.put("siteCode", "yc");
            obj.put("deviceNo", Util.getDeviceId(this));
            OkHttpHelper helper = new OkHttpHelper(this);

            helper.addPostRequest(new OkHttpHelper.CallbackLogic() {

                @Override
                public void onNetworkError(Request request, IOException e) {

                }

                @Override
                public void onBizSuccess(String responseDescription, final JSONObject data,
                                         final String flag) {
                    try {
                        Toast.makeText(getApplicationContext(), "Logout", Toast.LENGTH_SHORT).show();
                        S.setShare(kfme_WebBrowser.this, C.KEY_JSON_TOKEN,
                                // data.getString(C.KEY_JSON_TOKEN));
                                "");

                        S.set(kfme_WebBrowser.this, C.KEY_JSON_TOKEN,
                                // data.getString(C.KEY_JSON_TOKEN));
                                "");

                        S.set(kfme_WebBrowser.this, C.KEY_JSON_CUSTOM_CODE, "");
                        S.setShare(kfme_WebBrowser.this, C.KEY_JSON_CUSTOM_CODE, "");

                        S.set(kfme_WebBrowser.this, C.KEY_REQUEST_MEMBER_ID, "");
                        S.set(kfme_WebBrowser.this, C.KEY_JSON_CUSTOM_ID, "");
                        S.set(kfme_WebBrowser.this, C.KEY_JSON_NICK_NAME, "");
                        S.set(kfme_WebBrowser.this, C.KEY_JSON_PROFILE_IMG, "");
                        S.set(kfme_WebBrowser.this, C.KEY_JSON_DIV_CODE, "");
                        S.set(kfme_WebBrowser.this, C.KEY_JSON_SSOID, "");
                        S.set(kfme_WebBrowser.this, C.KEY_JSON_SSO_REGIKEY, "");
                        S.set(kfme_WebBrowser.this, C.KEY_JSON_MALL_HOME_ID, "");
                        S.set(kfme_WebBrowser.this, C.KEY_JSON_POINT_CARD_NO, "");
                        S.set(kfme_WebBrowser.this, C.KEY_JSON_PARENT_ID, "");
                        finish();

                    } catch (Exception e) {

                        L.e(e);

                    }

                }

                @Override
                public void onBizFailure(String responseDescription, JSONObject data,
                                         String responseCode) {

                }
            }, "http://api1.gigawon.cn:8088" +"/api/apiSSO/setLogout" , headers, obj.toString());
//										}, Constant.BASE_URL_SSO + Constant.SSO_LOGOUT,headers,obj.toString());

        } catch (Exception e) {
            e.getMessage();

        }

    }

    class SafeWebChromeClient extends WebChromeClient {

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            return super.onConsoleMessage(consoleMessage);
        }

        /**
         * 当前 WebView 加载网页进度
         *
         * @param view
         * @param newProgress
         */
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
        }

        /**
         * Js 中调用 alert() 函数，产生的对话框
         *
         * @param view
         * @param url
         * @param message
         * @param result
         * @return
         */
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {


            Log.i("xyz", "alert");
            return true;
        }

        /**
         * 处理 Js 中的 Confirm 对话框
         *
         * @param view
         * @param url
         * @param message
         * @param result
         * @return
         */
        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {

            Log.i("xyz", "Confirm");
            return super.onJsConfirm(view, url, message, result);
        }

        /**
         * 处理 JS 中的 Prompt对话框
         *
         * @param view
         * @param url
         * @param message
         * @param defaultValue
         * @param result
         * @return
         */
        @Override
        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {

            Log.i("xyz", "Prompt对话框");
            return super.onJsPrompt(view, url, message, defaultValue, result);
        }

        /**
         * 接收web页面的icon
         *
         * @param view
         * @param icon
         */
        @Override
        public void onReceivedIcon(WebView view, Bitmap icon) {
            super.onReceivedIcon(view, icon);
        }

        /**
         * 接收web页面的 Title
         *
         * @param view
         * @param title
         */
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
        }

    }

}



