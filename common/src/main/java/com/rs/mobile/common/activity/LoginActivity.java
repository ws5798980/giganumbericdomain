package com.rs.mobile.common.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rs.mobile.common.C;
import com.rs.mobile.common.L;
import com.rs.mobile.common.R;
import com.rs.mobile.common.S;
import com.rs.mobile.common.network.OkHttpHelper;
import com.rs.mobile.common.util.Util;
import com.tencent.android.tpush.XGIOperateCallback;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.XGPushManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;

import okhttp3.Request;

@SuppressLint("NewApi")
public class LoginActivity extends BaseActivity {

	// sms intent filter
	public static final String ACTION_RECEIVE_SMS = "android.provider.Telephony.SMS_RECEIVED";

	private LinearLayout closeBtn;

	private ImageView login_tryangle, membership_tryangle;

	private RelativeLayout loginTabBtn, membershipTabBtn;

	private LinearLayout loginLayout, membershipLayout;

	private EditText idEditText, pwEditText;

	private TextView loginBtn;

	private EditText idEditTextM, pwEditTextM, pwEditTextMCheck, smsEditText, et_parent_id;

	private TextView smsBtn, membershipBtn;
	
	private TextView app_ver_login, app_ver_memvership;

	private TextView forget_password;
	
	private Message m = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {

			setContentView(com.rs.mobile.common.R.layout.activity_login_rs);

			closeBtn = (LinearLayout) findViewById(com.rs.mobile.common.R.id.close_btn);
			closeBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					finish();

				}
			});

			loginTabBtn = (RelativeLayout) findViewById(com.rs.mobile.common.R.id.login_tab_btn);
			loginTabBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						Animation inUnderAnimation = AnimationUtils
								.loadAnimation(LoginActivity.this,
										com.rs.mobile.common.R.anim.scale_left_in);
						Animation outUnderAnimation = AnimationUtils
								.loadAnimation(LoginActivity.this,
										com.rs.mobile.common.R.anim.scale_left_out);
						Animation inLayoutAnimation = AnimationUtils
								.loadAnimation(LoginActivity.this,
										com.rs.mobile.common.R.anim.slide_left_in);
						Animation outLayoutAnimation = AnimationUtils
								.loadAnimation(LoginActivity.this,
										com.rs.mobile.common.R.anim.slide_left_out);

						//loginUnderLine.startAnimation(inUnderAnimation);
						//membershipUnderLine.startAnimation(outUnderAnimation);
						loginLayout.startAnimation(inLayoutAnimation);
						membershipLayout.startAnimation(outLayoutAnimation);

						//loginUnderLine.setVisibility(View.VISIBLE);
						//membershipUnderLine.setVisibility(View.GONE);
						login_tryangle.setVisibility(View.VISIBLE);
						loginLayout.setVisibility(View.VISIBLE);
						membership_tryangle.setVisibility(View.INVISIBLE);
						membershipLayout.setVisibility(View.GONE);

					} catch (Exception e) {

						L.e(e);

					}

				}
			});

			membershipTabBtn = (RelativeLayout) findViewById(com.rs.mobile.common.R.id.membership_tab_btn);
			membershipTabBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						Animation inUnderAnimation = AnimationUtils
								.loadAnimation(LoginActivity.this,
										com.rs.mobile.common.R.anim.scale_right_in);
						Animation outUnderAnimation = AnimationUtils
								.loadAnimation(LoginActivity.this,
										com.rs.mobile.common.R.anim.scale_right_out);
						Animation inLayoutAnimation = AnimationUtils
								.loadAnimation(LoginActivity.this,
										com.rs.mobile.common.R.anim.slide_right_in);
						Animation outLayoutAnimation = AnimationUtils
								.loadAnimation(LoginActivity.this,
										com.rs.mobile.common.R.anim.slide_right_out);

						//membershipUnderLine.startAnimation(inUnderAnimation);
						//loginUnderLine.startAnimation(outUnderAnimation);
						loginLayout.startAnimation(outLayoutAnimation);
						membershipLayout.startAnimation(inLayoutAnimation);

						//membershipUnderLine.setVisibility(View.VISIBLE);
						//loginUnderLine.setVisibility(View.GONE);

						loginLayout.setVisibility(View.GONE);
						login_tryangle.setVisibility(View.INVISIBLE);
						membership_tryangle.setVisibility(View.VISIBLE);
						membershipLayout.setVisibility(View.VISIBLE);

					} catch (Exception e) {

						L.e(e);

					}

				}
			});

			//loginUnderLine = (LinearLayout) findViewById(R.id.login_under_line);

			//membershipUnderLine = (LinearLayout) findViewById(R.id.membership_under_line);

			loginLayout = (LinearLayout) findViewById(com.rs.mobile.common.R.id.login_layout);

			membershipLayout = (LinearLayout) findViewById(com.rs.mobile.common.R.id.membership_layout);
			login_tryangle = (ImageView) findViewById(com.rs.mobile.common.R.id.Login_Tryangle);
			membership_tryangle = (ImageView) findViewById(com.rs.mobile.common.R.id.Member_Tryangle);

			idEditText = (EditText) findViewById(com.rs.mobile.common.R.id.id_edt_text);
			idEditText.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

					//checkLoginInput();

				}
			});

			pwEditText = (EditText) findViewById(com.rs.mobile.common.R.id.pw_edt_text);
			pwEditText.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

					//checkLoginInput();

				}
			});

			loginBtn = (TextView) findViewById(com.rs.mobile.common.R.id.login_btn);
			loginBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						final String id = idEditText.getText().toString();

						String pw = pwEditText.getText().toString();

						if (id == null || id.equals("")) {

							t(getString(com.rs.mobile.common.R.string.msg_empty_id));

							return;

						}

						if (pw == null || pw.equals("")) {

							t(getString(com.rs.mobile.common.R.string.msg_empty_pw));

							return;

						}

						HashMap<String, String> headers = new HashMap<>();
						headers.put("Content-Type", "application/json;Charset=UTF-8");
						HashMap<String, String> params = new HashMap<String, String>();
/*						params.put(C.KEY_REQUEST_MEMBER_ID, id);
						params.put(C.KEY_REQUEST_PW, encryption(pw));
						params.put(C.KEY_JSON_DEVICE_ID,Util.getDeviceId(LoginActivity.this));
						params.put(C.KEY_S_ID,"");
						params.put(C.KEY_VER, "1");
						params.put(C.KEY_LANG_TYPE, "chn");*/

						JSONObject j1=new JSONObject();
						try {

							j1.put(C.KEY_REQUEST_MEMBER_ID, id); //memid
							j1.put(C.KEY_REQUEST_PW, encryption(pw)); //mempwd
							j1.put(C.KEY_JSON_DEVICE_ID, Util.getDeviceId(LoginActivity.this));  //deviceNo
							j1.put(C.KEY_S_ID,""); //s_id
							j1.put(C.KEY_VER, "2"); //ver
							j1.put(C.KEY_LANG_TYPE, "chn");  //lang_type
							params.put("", j1.toString());

							//Jason Type : memid,mempwd,deviceNo, s_id, ver, lang_type

						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						OkHttpHelper helper = new OkHttpHelper(
								LoginActivity.this);
						helper.addPostRequest(new OkHttpHelper.CallbackLogic() {

							@Override
							public void onNetworkError(Request request,
									IOException e) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onBizSuccess(
									String responseDescription,
									JSONObject data, String flag) {
								// TODO Auto-generated method stub

								try {

									//Toast.makeText(getApplicationContext(), data.toString(), Toast.LENGTH_LONG).show();

									if (flag.equals(C.VALUE_RESPONSE_SUCCESS) || flag.equals(C.VALUE_RESPONSE_SUCCESS_NUM)) {

										if (C.INTERFACE_PARAMS == null) {

											C.INTERFACE_PARAMS = new HashMap<String, String>();

										}

										C.INTERFACE_PARAMS.clear();

										Iterator<String> keys = data.keys();

										while (keys.hasNext()) {

											String key = keys.next();
											String value = data.getString(key);

											C.INTERFACE_PARAMS.put(key, value);

										}

										// S.set(LoginActivity.this,
										// C.KEY_JSON_TOKEN,
										// data.getString(C.KEY_JSON_TOKEN));

										//添加DEVICEID 加入token
										String deviceid = Util.getDeviceId(LoginActivity.this) ;
										if(deviceid != null && !deviceid.equals("")){
											S.setShare(LoginActivity.this, C.KEY_JSON_TOKEN, data.getString(C.KEY_JSON_TOKEN) + "|"  + deviceid);
										}else{
											S.setShare(LoginActivity.this, C.KEY_JSON_TOKEN, data.getString(C.KEY_JSON_TOKEN));
										}

//										S.setShare(LoginActivity.this, C.KEY_JSON_TOKEN, data.getString(C.KEY_JSON_TOKEN));

//										S.setShare(LoginActivity.this, C.KEY_REQUEST_MEMBER_ID, id);

										S.setShare(LoginActivity.this, C.KEY_REQUEST_MEMBER_ID, data.getString("memid"));

										if (data.has("custom_id"))
											S.setShare(LoginActivity.this, C.KEY_SHARED_PHONE_NUMBER, data.getString("custom_id"));

										setResult(RESULT_OK);

										xgPush();
										finish();

									}

									t(data.getString("msg"));

									//L.d("onBizSuccess : " + data.toString());


								} catch (Exception e) {
									L.e(e);
								}

							}

							@Override
							public void onBizFailure(
									String responseDescription,
									JSONObject data, String responseCode) {
								//Toast.makeText(getApplicationContext(), data.toString(), Toast.LENGTH_LONG).show();
								// TODO Auto-generated method stub

							}
						}, C.BASE_MEMBER_RS_URL + C.LOGIN_ENC_RS_URL, headers, j1.toString());
								//http://member.osunggiga.com:8800/api/Login/memberLogin
					} catch (Exception e) {

						L.e(e);

					}

				}
			});

			idEditTextM = (EditText) findViewById(com.rs.mobile.common.R.id.id_edt_text_m);
			idEditTextM.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

					checkMembershipInput();

				}
			});

			pwEditTextM = (EditText) findViewById(com.rs.mobile.common.R.id.pw_edt_text_m);
			pwEditTextM.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

					checkMembershipInput();

				}
			});
			
			/*pwEditTextMCheck = (EditText) findViewById(R.id.pw_edt_text_m_check);
			pwEditTextMCheck.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

					checkMembershipInput();

				}
			});*/

			smsEditText = (EditText) findViewById(com.rs.mobile.common.R.id.sms_edt_text_m);
			smsEditText.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

					checkMembershipInput();

				}
			});

			smsBtn = (TextView) findViewById(com.rs.mobile.common.R.id.sms_btn);
			smsBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						String id = idEditTextM.getText().toString();

						if (id == null || id.equals("")) {

							if (id == null || id.equals("")) {

								t(getString(com.rs.mobile.common.R.string.msg_empty_id));

								return;

							}
							
							return;

						}

						HashMap<String, String> params = new HashMap<String, String>();
						params.put(C.KEY_REQUEST_HN, id);

						OkHttpHelper helper = new OkHttpHelper(
								LoginActivity.this);
						helper.addPostRequest(new OkHttpHelper.CallbackLogic() {

							@Override
							public void onNetworkError(Request request,
									IOException e) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onBizSuccess(
									String responseDescription,
									JSONObject data, String flag) {
								// TODO Auto-generated method stub

								try {

									t(data.getString("msg"));

									L.d("onBizSuccess : " + data.toString());

								} catch (Exception e) {

									L.e(e);

								}

							}

							@Override
							public void onBizFailure(
									String responseDescription,
									JSONObject data, String responseCode) {
								// TODO Auto-generated method stub

							}
						}, C.BASE_URL + C.GET_AUTH_NUMBER_URL, params);
						//http://portal.osunggiga.com:8488/Member/SMSAuthNumSend

					} catch (Exception e) {

						L.e(e);

					}

				}
			});

			et_parent_id = (EditText) findViewById(R.id.parent_id);


			membershipBtn = (TextView) findViewById(com.rs.mobile.common.R.id.memership_btn);
			membershipBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						String id = idEditTextM.getText().toString();

						String pw = pwEditTextM.getText().toString();
						
						//String pwCheck = pwEditTextMCheck.getText().toString();

						String an = smsEditText.getText().toString();

						if (id == null || id.equals("")) {

							t(getString(com.rs.mobile.common.R.string.msg_empty_id));

							return;

						}

						if (pw == null || pw.equals("")) {

							t(getString(com.rs.mobile.common.R.string.msg_empty_pw));

							return;

						}
						
						/*if (pwCheck == null || pwCheck.equals("")) {

							t(getString(R.string.msg_pls_input_yourpassword_check));

							return;

						}*/

						if (an == null || an.equals("")) {

							t(getString(com.rs.mobile.common.R.string.msg_empty_sms));

							return;

						}

						/*if (et_parent_id.getText().toString().isEmpty())
						{
							t(getString(com.rs.mobile.common.R.string.msg_empty_id));

							return;
						}*/
						
						/*if (!pw.equals(pwCheck)) {
							
							t(getString(R.string.msg_equals_pw));
							
							return;
						}*/

						/*
						수정일 / 수정인 : 2017.12.25 Jack Kim
						회원가입시 변경된 API로 Request Parameter를 던질때 HPNum과 mem_id를 같이 던져야 한다
						HPNum은 Push 서버로 보낼 때 해당 파라미터로 되어 있기 때문이다
						 */
						HashMap<String, String> params = new HashMap<String, String>();

						params.put(C.KEY_REQUEST_HN, id);
						params.put(C.KEY_REQUEST_MEMID, id);
						params.put(C.KEY_REQUEST_PW, encryption(pw));
						params.put(C.KEY_REQUEST_AN, an);
						params.put(C.KEY_VER,"2");
						params.put(C.KEY_REQUEST_PARENT_ID,et_parent_id.getText().toString());

						OkHttpHelper helper = new OkHttpHelper(
								LoginActivity.this);
						helper.addPostRequest(new OkHttpHelper.CallbackLogic() {

							@Override
							public void onNetworkError(Request request,
									IOException e) {
								// TODO Auto-generated method stub
								Util.Debug_Toast_Message(getApplicationContext(), e.toString());
								//Util.Real_Service_Toast_Message(getApplicationContext(), "通信有问题");
							}

							@Override
							public void onBizSuccess(
									String responseDescription,
									JSONObject data, String flag) {
								// TODO Auto-generated method stub

								try {
								
									if (flag.equals(C.VALUE_RESPONSE_SUCCESS)) {  //"text success"
	
//										showDialog(getString(R.string.title_pay_pw), getString(R.string.msg_pay_pw), getString(R.string.ok), 
//												new OnClickListener() {
//													
//													@Override
//													public void onClick(View v) {
//														// TODO Auto-generated method stub
//														
//														D.alertDialog.dismiss();
														
														Animation inUnderAnimation = AnimationUtils
																.loadAnimation(LoginActivity.this,
																		com.rs.mobile.common.R.anim.scale_left_in);
														Animation outUnderAnimation = AnimationUtils
																.loadAnimation(LoginActivity.this,
																		com.rs.mobile.common.R.anim.scale_left_out);
														Animation inLayoutAnimation = AnimationUtils
																.loadAnimation(LoginActivity.this,
																		com.rs.mobile.common.R.anim.slide_left_in);
														Animation outLayoutAnimation = AnimationUtils
																.loadAnimation(LoginActivity.this,
																		com.rs.mobile.common.R.anim.slide_left_out);
					
/*														loginUnderLine
																.startAnimation(inUnderAnimation);
														membershipUnderLine
																.startAnimation(outUnderAnimation);*/
														loginLayout
																.startAnimation(inLayoutAnimation);
														membershipLayout
																.startAnimation(outLayoutAnimation);
					
/*														loginUnderLine.setVisibility(View.VISIBLE);
														membershipUnderLine
																.setVisibility(View.GONE);*/
														login_tryangle.setVisibility(View.VISIBLE);
														loginLayout.setVisibility(View.VISIBLE);
														membership_tryangle.setVisibility(View.INVISIBLE);
														membershipLayout.setVisibility(View.GONE);
//													}
//												});
	
									}
	
									t(data.getString("msg"));
	
									L.d("onBizSuccess : " + data.toString());
								
								} catch (Exception e) {
									
									L.e(e);
									
								}

							}

							@Override
							public void onBizFailure(
									String responseDescription,
									JSONObject data, String responseCode) {
								Util.Debug_Toast_Message(getApplicationContext(), responseDescription);
								// TODO Auto-generated method stub

							}
						}, C.BASE_MEMBER_RS_URL + C.REGIST_MEMBERSHIP_ENC_URL, params);

					} catch (Exception e) {

						L.e(e);

					}
				}
			});
			
			app_ver_login = (TextView)findViewById(com.rs.mobile.common.R.id.app_ver_login);
			
			forget_password=(TextView)findViewById(com.rs.mobile.common.R.id.forget_password);
			forget_password.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
			forget_password.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this,ForgetPassWordActivity.class));	
				}
			});
			app_ver_memvership = (TextView)findViewById(com.rs.mobile.common.R.id.app_ver_memvership);

			try {

				// 자기 전화번호 가져오기
				TelephonyManager telephone = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
				String phoneNumber = telephone.getLine1Number();

				L.d("number : " + telephone.getSimSerialNumber());
				L.d("number : " + telephone.getLine1Number());

				if (phoneNumber != null) {

					// 중국
					if (phoneNumber.contains("+86"))
						phoneNumber = phoneNumber.replace("+86", "");

					// 한국
					if (phoneNumber.contains("+82")) {

						phoneNumber = phoneNumber.replace("+82", "");
						phoneNumber = "0" + phoneNumber;
					}

				} else {
					phoneNumber = "";
				}

				idEditText.setText(phoneNumber);
				idEditTextM.setText(phoneNumber);

			} catch (Exception e) {

				L.e(e);

			}
			
			//버전 가져오기
			try {
				
				PackageManager pm = getPackageManager();
				
				PackageInfo info = null;
			
				info = pm.getPackageInfo(getApplicationContext().getPackageName(), 0);
				
				if (info != null) {
					
					app_ver_login.setText(getResources().getString(com.rs.mobile.common.R.string.app_name) +
							"Version : v" + info.versionName + "(" + (C.BASE_URL.contains("portal.osunggiga.com")? "P":"D") + ")");
					
					app_ver_memvership.setText(getResources().getString(com.rs.mobile.common.R.string.app_name) +
							"Version : v" + info.versionName + "(" + (C.BASE_URL.contains("portal.osunggiga.com")? "P":"D") + ")");
					
				}
				
			} catch (Exception e) {
				
				L.e(e);
				
			}

		} catch (Exception e) {

			e(e);

		}

	}

	/**
	 * 2017.12.22 Jack Kim
	 * checkLoginInput
	 * 암호입력 EditText를 클릭하면 실행되는 함수
	 * Background가 변경된다
	 */
	public void checkLoginInput() {

		try {

			String id = idEditText.getText().toString();

			String pw = pwEditText.getText().toString();

			if (id != null && id.length() > 0 && pw != null && pw.length() > 0) {

				loginBtn.setBackgroundDrawable(getResources().getDrawable(
						com.rs.mobile.common.R.drawable.bg_login_n_btn));

			} else {

				loginBtn.setBackgroundDrawable(getResources().getDrawable(
						com.rs.mobile.common.R.drawable.bg_login_d_btn));

			}

		} catch (Exception e) {

			e(e);

		}

	}

	/**
	 * checkMembershipInput
	 */
	public void checkMembershipInput() {

		try {

			String id = idEditTextM.getText().toString();

			String pw = pwEditTextM.getText().toString();
			
			String pwCheck = pwEditTextMCheck.getText().toString();

			String sms = smsEditText.getText().toString();

			if (id != null && id.length() > 0 && pw != null && pw.length() > 0
					&& pwCheck != null && pwCheck.length() > 0 && sms != null && sms.length() > 0) {

				membershipBtn.setBackgroundDrawable(getResources().getDrawable(
						com.rs.mobile.common.R.drawable.bg_login_n_btn));

			} else {

				membershipBtn.setBackgroundDrawable(getResources().getDrawable(
						com.rs.mobile.common.R.drawable.bg_login_d_btn));

			}

		} catch (Exception e) {

			e(e);

		}

	}

	/**
	 * receiver
	 * 
	 * @author ZZooN
	 * 
	 */
	public class receiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			try {

				if (intent.getAction().equals(ACTION_RECEIVE_SMS)) {

					Bundle bundle = intent.getExtras();

					String message = "";

					if (bundle != null) {

						Object[] pdus = (Object[]) bundle.get("pdus");

						for (Object pdu : pdus) {

							SmsMessage smsMessage = SmsMessage
									.createFromPdu((byte[]) pdu);
							message += smsMessage.getMessageBody();

						}

					}

					// if (message != null && mainFragment.phoneRequestNumber !=
					// null) {
					//
					// if (message.contains(mainFragment.phoneRequestNumber)) {
					//
					// T.showToast(MainActivity.this,
					// R.string.msg_cert_sms_completed);
					//
					// mainFragment.rPhoneEditTextC.setText(mainFragment.phoneRequestNumber);
					//
					// }
					//
					// }

				}

			} catch (Exception e) {

				L.e(e);

			}

		}

	}

	private String encryption(String userPassword) {

		MessageDigest md;

		String encritPassword = "";

		try {
			md = MessageDigest.getInstance("SHA-512");

			md.update(userPassword.getBytes());
			byte[] mb = md.digest();
			for (int i = 0; i < mb.length; i++) {
				byte temp = mb[i];
				String s = Integer.toHexString(new Byte(temp));
				while (s.length() < 2) {
					s = "0" + s;
				}
				s = s.substring(s.length() - 2);
				encritPassword += s;
			}

		} catch (Exception e) {

			e(e);
			
		}

		return encritPassword;
	}
	/**
	 * 푸시 요청
	 */
	private void xgPush() {

		XGPushConfig.enableDebug(this, true);
		Handler handler = new HandlerExtension();
		m = handler.obtainMessage();
		// 注册接口

		XGPushManager.registerPush(this, S.getShare(this, C.KEY_SHARED_PHONE_NUMBER,""), new XGIOperateCallback() {

			@Override
			public void onSuccess(Object data, int flag) {
				d("+++ register push sucess. token:" + data);
				m.obj = "+++ register push sucess. token:" + data;
				m.sendToTarget();
				
			}

			@Override
			public void onFail(Object data, int errCode, String msg) {
				d("+++ register push fail. token:" + data + ", errCode:" + errCode + ",msg:" + msg);

				m.obj = "+++ register push fail. token:" + data + ", errCode:" + errCode + ",msg:" + msg;
				m.sendToTarget();
				
			}
		});

	}
	private static class HandlerExtension extends Handler {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
		}
	}
}
